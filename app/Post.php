<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    protected $fillable = ['title', 'resume', 'description', 'location', 'image', 'video',  'tags', '$id_user'];

    public function user()
    {
        return $this->belongsTo("App\User", "id")->withTrashed();
    }

    use SoftDeletes;
}
