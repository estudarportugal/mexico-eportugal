<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Testimonial extends Model
{


    protected $fillable = ['name', 'description', 'photo'];
    protected $table = 'testimonials';

}
