<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Landingpage extends Model
{
    protected $fillable = ['title', 'description', 'tags', 'backgroundimage1', 'backgroundimage2','contact1', 'contact2'];
    protected $table = 'landingpages';
}
