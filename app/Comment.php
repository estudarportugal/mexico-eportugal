<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Comment extends Model
{
    protected $primaryKey = "id";
    protected $fillable = ['status','name', 'email','comment','id_post'];
    protected $table = 'comments';

    public function pubtitle()
    {
        if ($this->id_post == null){
            return 'N/A';
        }else{
            $pubtitle = Post::where('id', $this->id_post)->first()->title;
            return $pubtitle;
        }
    }
}
