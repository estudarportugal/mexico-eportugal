<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendAccountConfirmation extends Mailable
{
    use Queueable, SerializesModels;

    public $userid;
    public $username;

    public function __construct(string $userid, string $username){
      $this->userid = $userid;
      $this->username = $username;
    }

    public function build(){
      return $this->markdown('layouts.mail')
      ->subject('Ativação da Conta | Estudar Portugal')
      ->with([
                'name' => $this->username,
                'link' => 'http://mexico.estudarportugal.com/accountconfirmation/'.$this->userid
            ]);
    }
}
