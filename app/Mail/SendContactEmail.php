<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendContactEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $name;
    public $email;
    public $phone;
    public $academicStatus;
    public $currentSchool;
    public $test;


    public function __construct($name, $email, $phone, $academicStatus, $currentSchool, $test){
      $this->name = $name;
      $this->email = $email;
      $this->phone = $phone;
      $this->academicStatus = $academicStatus;
      $this->currentSchool = $currentSchool;
      $this->test = $test;

      $this->from($email);
      $this->subject('Nova mensage de '.$name.' | México - Estudar Portugal');
    }


    public function build(){
      return $this->view('layouts.contactmail');
    }
}
