<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTestimonialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            "name" => 'required',
            "description" => 'required'
        ];
    }

    public function messages()
    {
        return [

            'name.required' => 'O nome tem de ser preenchido',
            'description.required' => 'É necessário escrever o testemunho'
        ];
    }
}
