<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => 'required|max:150',
            "image" => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            "location" => 'nullable',
            "resume" => 'required|max:150',
            "tags" => 'nullable',
            "description" => 'required|min:3'
        ];

    }
    public function messages()
    {
        return [
            'title.required' => 'O Título é obrigatório ser preenchido.',
            'resume.required' => 'O Texto Curto é obrigatório ser preenchido.',
            'description.required' => 'A Descrição é obrigatória ser preenchida.'
        ];
    }
}
