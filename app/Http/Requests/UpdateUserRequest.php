<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "name" => 'required|regex:/^[A-ZÀ-úa-z\s]+$/',
            "email" => 'required|email|unique:users,email,' . $this->user->id,
            "role" => 'nullable|in:Admin,Employer'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'O Nome é obrigatório ser preenchido.',
            'email.required' => 'O Email é obrigatório ser preenchido.',
            'email.email' => 'O Email é tem te conter o @ e ter de existir.'
        ];
    }
}
