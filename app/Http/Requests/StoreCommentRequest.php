<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules() {
    return [
        "id_post" => 'required',
        "name" => 'required|min:3|max:20',
        "email" => 'required',
        "comment" => 'required'
    ];
    }


    public function messages(){
    return [
        'name.required' => 'O nome é de preechimento obrigatório',
        'email.required' => 'Introduza um e-mail válido',
        'comment.required' => 'Introduza o seu comentário'
    ];
    }
}
