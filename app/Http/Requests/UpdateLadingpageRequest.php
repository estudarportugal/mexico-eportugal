<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateLadingpageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => 'required|max:150',
            "backgroundimage1" => 'nullable|image|mimes:jpeg,png,jpg,gif|max:3048',
            "backgroundimage2" => 'nullable|image|mimes:jpeg,png,jpg,gif|max:3048',
            "description" => 'required|max:150',
            "tags" => 'nullable',
            "contact1" => 'nullable',
            "contact2" => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'O Título é obrigatório ser preenchido.',
            'description.required' => 'A Descrição é obrigatória ser preenchida.'
        ];
    }
}
