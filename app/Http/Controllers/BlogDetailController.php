<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlogDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($post)
    {
        $post = Post::findOrFail($post);

        /* select COUNT(*) AS "quant" from comments WHERE comments.status LIKE "Aprovado" */
        $totalcomments = DB::table("comments")
        ->select(DB::raw("COUNT(*) as quant"))
        ->where('id_post', $post->id)
        ->where('status', 'like', 'Aprovado')
        ->first();

        $comments = DB::table('comments')
        ->select('*')
        ->where('id_post', $post->id)
        ->where('status', 'like', 'Aprovado')
        ->get();

        return view('frontoffice.blog-detail', compact('post','totalcomments','comments'));
    }
}
