<?php

namespace App\Http\Controllers;


use App\Http\Requests\UpdatePostRequest;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Requests\StorePostRequest;
use Auth;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        return view('backoffice/posts.list', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $post = new Post;
        return view('backoffice.posts.add', compact('post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request)
    {
        $fields = $request->validated();

        $post = new Post;
        $post->fill($fields);

        $post->id_user = auth()->user()->id;

        // Data em que o registo é criado
        $t = time();
        $post->create_at == date("Y-m-d", $t);

        $post->save();

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $postImg = $post->id . '.' . $image->getClientOriginalExtension();
            Storage::disk('public')->putFileAs('posts-images/', $image, $postImg);
            $post->image = $postImg;

        }

        if ($request->hasFile('video')) {
            $video= $request->file('video');
            $postVideo = $post->id . '.' . $video->getClientOriginalExtension();
            Storage::disk('public')->putFileAs('posts-videos/', $video, $postVideo);
            $post->video = $postVideo;

        }
        $post->save();

        return redirect()->route('posts.index')->with('success', 'Divulgação Adicionada com Sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('backoffice.posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('backoffice.posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, Post $post)
    {
        $fields = $request->validated();
        $post->fill($fields);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $postImg = $post->id . '_' . time() . '.' . $image->getClientOriginalExtension();

            if (!empty($post->image)) {
                Storage::disk('public')->delete('posts-images/' . $post->image);
            }

            Storage::disk('public')->putFileAs('posts-images/', $image, $postImg);
            $post->image = $postImg;
        }

        if ($request->hasFile('video')) {
            $video = $request->file('video');
            $postVideo = $post->id . '_' . time() . '.' . $video->getClientOriginalExtension();
            if (!empty($post->video)) {
                Storage::disk('public')->delete('posts-videos/' . $post->video);
            }
            Storage::disk('public')->putFileAs('posts-videos/', $video, $postVideo);
            $post->video = $postVideo;
        }
        $post->save();

        // Data em que o registo é modificado
        $t = time();
        $post->updated_at == date("Y-m-d", $t);
        $post->save();

        return redirect()->route('posts.index')->with('success', 'Divulgação Editada com Sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if (!empty($post->image)) {
            Storage::disk('public')->delete('posts-images/' . $post->image);
        }

        $post->delete();

        return redirect()->route('posts.index')->with('success', 'Divulgação Eliminada com Sucesso!');
    }
}
