<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use App\Mail\SendContactEmail;

class ContactController extends Controller
{
    public function index(){
        return view('frontoffice.contacts');
    }

    public function sendMail(Request $request){
      $this->validate($request, [
        'name' => 'required',
        'email' => 'required|email',
        'phone' => 'required',
        'academicStatus' => 'required',
        'currentSchool' => 'required',
        'test' => 'required',
      ]);

      Mail::to('test@estudarportugal.com')->send(new SendContactEmail($request->get('name'), $request->get('email'), $request->get('phone'), $request->get('academicStatus'), $request->get('currentSchool'), $request->get('test')));
      return redirect()->route('contacts')->with('success', 'Mensagem enviada com sucesso!');
    }
}
