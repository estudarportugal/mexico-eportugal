<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){

        // TOTAL DE VISITAS
        $totalvisitas = DB::table("views")
        ->select(DB::raw("COUNT(*) as quant"))
        ->first();




        // Consulta o numero de visitas ao site
        $visitasweb = DB::table("views")
        ->select(DB::raw("COUNT(*) as quant"))
        ->where('views.origin', '=', "Website")
        ->first();


        // Consulta o numero de visitas à LandingPage
        $visitaslanding = DB::table("views")
        ->select(DB::raw("COUNT(*) as quant"))
        ->where('views.origin', '=', "Landingpage")
        ->first();



        // Consulta o numero de visitas à LandingPage
        $totalcomments = DB::table("comments")
        ->select(DB::raw("COUNT(*) as quant"))
        ->first();


        // Consulta o numero de comentários por aprovar
        $penddingcomments = DB::table("comments")
        ->select(DB::raw("COUNT(*) as quant"))
        ->where('comments.status', '=', "Pendente")
        ->first();




        // TOTAL DE publicações
        $totalpubs = DB::table("posts")
        ->select(DB::raw("COUNT(*) as quant"))
        ->first();


        
        // Locais de onde estão a ser feitos os acessos
        //SELECT LOCAL, COUNT(*) AS quant FROM views GROUP BY LOCAL ORDER BY LOCAL ASC
        $geovisits = DB::table('views')
        ->selectRaw("views.local, COUNT(*) AS quant")
        ->groupBy('views.local')
        ->orderBy('views.local','asc')
        ->get();

        return view('backoffice.dashboard',compact('visitasweb','visitaslanding','totalvisitas','penddingcomments','totalcomments','totalpubs','geovisits'));
    }

    public function ajuda(){
      return view('backoffice.help');
    }


}
