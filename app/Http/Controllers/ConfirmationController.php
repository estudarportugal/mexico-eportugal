<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UpdatePasswordRequest;

class ConfirmationController extends Controller
{
    public function index($user){
      $user = User::findOrFail($user);

      return view('auth.verify', compact('user'));
    }

    public function edit(User $user){
      return view('auth.verify', compact('user'));
    }

    public function update(UpdatePasswordRequest $request, User $user){
      $fields=$request->validated();
      $user->fill($fields);
      $password = $request->input('password');
      $hashed = Hash::make($password);
      $user->password = $hashed;
      $user->save();
      return redirect()->route('login')->with('success', 'Password inserida com sucesso!');
    }
}
