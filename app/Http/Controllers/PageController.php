<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PageController extends Controller
{
    public function index(){


      $ip_address=$_SERVER['REMOTE_ADDR'];

      $query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip_address));
       if($query && $query['status'] == 'success')
      {
        DB::table('views')->insert( ['origin'=>"Website",'local'=>$query['country'],'created_at' => date("Y-m-d",time())] );
      }


      return view('frontoffice/index');
    }

    public function acerca(){
      return view('frontoffice/about');
    }
}
