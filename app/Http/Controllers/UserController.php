<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\StoreUserRequest;
use App\Mail\SendAccountConfirmation;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Database\Eloquent\SoftDeletes;


class UserController extends Controller{

    public function index(){
      if (Auth::user()->role == "Admin") {
        $users = User::orderBy('name', 'asc')
        ->get();
        return view('backoffice.users.list', compact('users'));
      }else {
        abort(403);
      }
    }

    public function create(){
      if (Auth::user()->role == "Admin") {
        $user = new User;
        return view('backoffice.users.add', compact('user'));
      }else {
        abort(403);
      }
    }

    public function store(StoreUserRequest $request){
      $fields = $request->validated();
      $user = new User();
      $user->fill($fields);
      $user->password = Hash::make('?_estudarP0rtugalMX!P4ssword');
      $user->save();
      $userid = $user->id;
      $username = $user->name;
      Mail::to($user->email)->send(new SendAccountConfirmation($userid, $username));
      return redirect()->route('users.index')->with('success', 'Utilizador criado com sucesso!');
    }

    public function show(User $user){
      if (Auth::user()->role == "Admin") {
        return view('backoffice.users.show', compact('user'));
      }else {
        abort(403);
      }
    }

    public function edit(User $user){
      if (Auth::user()->role == "Admin") {
        return view('backoffice.users.edit', compact('user'));
      }else {
        abort(403);
      }
    }

    public function update(UpdateUserRequest $request, User $user){
      $fields=$request->validated();
      $user->fill($fields);
      $user->save();
      return redirect()->route('users.index')->with('success', 'Utilizador editado com sucesso!');
    }

    public function destroy(User $user){
      $user->delete();
      return redirect()->route('users.index')->with('success', 'Utilizador eliminado com sucesso!');;
    }
}
