<?php

namespace App\Http\Controllers;

use App\Testimonial;
use Illuminate\Http\Request;
use App\Http\Requests\StoreTestimonialRequest;
use App\Http\Requests\UpdateTestimonialRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;


class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimonials = Testimonial::all();
        return view('backoffice.testimonials.list', compact('testimonials'));
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $testimonial = new Testimonial;
        return view('backoffice.testimonials.add', compact('testimonial'));
    }





    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTestimonialRequest $request)
    {
        $fields = $request->validated();
        $testimonial = new Testimonial;
        $testimonial->fill($fields);


        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $studentphoto = $testimonial->name . '_' . time() . '.' . $photo->getClientOriginalExtension();
            Storage::disk('public')->putFileAs('testimonials-photos/', $photo, $studentphoto);
            $testimonial->photo = $studentphoto;
            $testimonial->save();
        }

        // data em que foi criado
        $t=time();
        $testimonial->create_at == date("Y-m-d",$t);

        $testimonial->save();

        return redirect()->route('testimonials.index')->with('success', 'Testemunho criado com sucesso ');
    }




    /**
     * Display the specified resource.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function show(Testimonial $testimonial)
    {
        return view('backoffice.testimonials.show', compact('testimonial'));
    }





    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function edit(Testimonial $testimonial)
    {
        return view('backoffice.testimonials.edit', compact('testimonial'));
    }






    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTestimonialRequest $request, Testimonial $testimonial)
    {
        $fields = $request->validated();
        $testimonial->fill($fields);


        if ($request->photo==null){
            $testimonial->photo = "default_user.png";
        }

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $studentphoto = $testimonial->id . '_' . time() . '.' . $photo->getClientOriginalExtension();
            if (!empty($testimonial->photo)) {
                Storage::disk('public')->delete('testimonials-photos/' . $testimonial->photo);
            }
            Storage::disk('public')->putFileAs('testimonials-photos/', $photo, $studentphoto);
            $testimonial->photo = $studentphoto;
        }

        // data em que foi modificado
        $t=time();
        $testimonial->updated_at == date("Y-m-d",$t);

        $testimonial->save();


            return redirect()->route('testimonials.index')->with('success', 'Testemunho modificado com sucesso');
 
    }






    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function destroy(Testimonial $testimonial)
    {
        $testimonial->delete();
        return redirect()->route('testimonials.index')->with('success', 'Testemunho eliminado com sucesso');
    }
}
