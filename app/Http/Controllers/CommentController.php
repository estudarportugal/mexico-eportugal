<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use App\Http\Requests\StoreCommentRequest;
use Illuminate\Support\Facades\DB;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::all();

        return view('backoffice.comments.list',compact('comments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(StoreCommentRequest $request) 
    {
        
        $fields = $request->validated();
        $comment = new Comment;
        $comment->fill($fields);
        $comment->status = "Pendente";
        

        // Data em que o registo é criado
        $t = time();
        $comment->create_at == date("Y-m-d", $t);
        $comment->save();

        return redirect()->back()->with('success', 'Comentário enviado com sucesso. Aguarda aprovação');
        /* $service->notify(new alertMessage($request->get('name'),$request->get('subject'))); */
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        
        $posttitle = DB::table('posts')
        ->select('*')
        ->where('id', $comment->id_post)
        ->first();

        return view('backoffice.comments.show',compact('comment','posttitle'));
  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        $comment->status="Aprovado";
        $comment->save();
        return redirect()->route('comments.index')->with('success', 'Comentário aprovado com sucesso');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();
        return redirect()->route('comments.index')->with('success', 'Comentário eliminado com sucesso');
    }
}
