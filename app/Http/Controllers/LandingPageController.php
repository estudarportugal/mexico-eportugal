<?php

namespace App\Http\Controllers;

use App\Landingpage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\UpdateLadingpageRequest;
use Illuminate\Support\Facades\Storage;
use Auth;

class LandingpageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $landingpage = DB::table('landingpages')->first();


        $testimonials = DB::table('testimonials')->get();
        


        $ip_address=$_SERVER['REMOTE_ADDR'];

        $query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
         if($query && $query['status'] == 'success')
        {
          DB::table('views')->insert( ['origin'=>"Landingpage",'local'=>$query['country'],'created_at' => date("Y-m-d",time())] );
        }


        return view('frontoffice.landing-page.landingPage', compact('landingpage','testimonials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Landingpage $landingpage
     * @return \Illuminate\Http\Response
     */
    public function show(Landingpage $landingpage)
    {
/*         $landingpage = DB::table('landingpages')->first();
        return view('frontoffice.landing-page.landingPage', compact('landingpage')); */
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Landingpage $landingpage
     * @return \Illuminate\Http\Response
     */
    public function edit(Landingpage $landingpage)
    {
        return view('backoffice.landingpages.edit', compact('landingpage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Landingpage $landingpage
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLadingpageRequest $request, Landingpage $landingpage)
    {
        $fields = $request->validated();

        $landingpage->fill($fields);

        if ($request->hasFile('backgroundimage1')) {
            $backgroundimage1 = $request->file('backgroundimage1');
            $postImg1 = $landingpage->id . '_' . time() . '.' . $backgroundimage1->getClientOriginalExtension();

            if (!empty($landingpage->backgroundimage1)) {
                Storage::disk('public')->delete('landingpages-images/' . $landingpage->backgroundimage1);
            }

            Storage::disk('public')->putFileAs('landingpages-images/', $backgroundimage1, $postImg1);
            $landingpage->backgroundimage1 = $postImg1;
        }

        if ($request->hasFile('backgroundimage2')) {
            $backgroundimage2 = $request->file('backgroundimage2');
            $postImg2 = $landingpage->id . '_' . time() . '.' . $backgroundimage2->getClientOriginalExtension();

            if (!empty($landingpage->backgroundimage2)) {
                Storage::disk('public')->delete('landingpages-images/' . $landingpage->backgroundimage2);
            }

            Storage::disk('public')->putFileAs('landingpages-images/', $backgroundimage2, $postImg2);
            $landingpage->backgroundimage2 = $postImg2;
        }

        // Data em que o registo é modificado
        $t = time();
        $landingpage->updated_at == date("Y-m-d", $t);
        $landingpage->save();

        return redirect()->route('landingpages.edit', 1)->with('success', 'Landing Page Editada com Sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Landingpage $landingpage
     * @return \Illuminate\Http\Response
     */
    public function destroy(Landingpage $landingpage)
    {
        //
    }
}
