<!DOCTYPE html>
<html lang="pt" dir="ltr">

<head>
    <meta charset="utf-8">
</head>

<body bgcolor="#f2f2f2" style="font-family: Helvetica">
    <h3>Nova Mensagem de {{ $name }}</h3>
    <span style="font-size:14px">
        <p> <b>E-mail:</b> {{ $email }} </p>
        <p> <b>Telemóvel:</b> {{ $phone }} </p>
        <p> <b>Nível Académico:</b> {{ $academicStatus }} </p>
        <p> <b>Escola Atual:</b> {{ $currentSchool }} </p>
        <p> <b>Mensagem:</b> </p>
        <div style="background-color: white; padding: 15px; white-space: pre-wrap">{{ $test }}</div>
        <br>
        <hr>
        <p style="text-align:center;">México - Estudar Portugal {{ "© ".date("Y") }}</p>
    </span>
</body>

</html>
