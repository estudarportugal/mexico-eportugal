@component('mail::message')
Olá **{{$name}}**!  {{-- use double space for line break --}}

Para aceder a sua conta **Estudar Portugal** basta criar a sua própria password, clicando no botão abaixo indicado.
@component('mail::button', ['url' => $link])
Aceder a conta
@endcomponent
Bom trabalho,
Estudar Portugal | México.
@endcomponent
