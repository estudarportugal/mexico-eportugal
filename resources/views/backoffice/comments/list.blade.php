@extends('backoffice.master-back')

@section('title', 'Comentários')


@section('styleLinks')
<link href="{{asset('css/style-backoffice-comments.css')}}" rel="stylesheet">
@endsection


@section('content')

@include('backoffice.comments.partials.modal')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Comentários do Blog</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{route('home')}}" class="text-dark">Dashboard</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Comentários do Blog</li>
                    </ol>
                </nav>
            </div>
        </div>
       {{--  <div class="col-5 text-right ">
            Aprovar automáticamente: <select class="custom-select" style="width: 70px">
                <option>Sim</option>
                <option selected>Não</option>
            </select>
        </div> --}}
    </div>
</div>


<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">



                    <div class="table-responsive" style="overflow:hidden;">
                        <table id="dataTable" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    {{-- <th class="text-center">ID#</th> --}}
                                    <th>Data</th>
                                    <th>Remetente</th>
                                    {{-- <th>E-mail</th> --}}
                                    <th>Publicação</th>
                                    <th>Estado</th>
                                    <th class="text-center" style="width: 15px">Opções</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($comments!==null)
                                @foreach($comments as $comment)
                                <tr>
                                    {{-- <td class="text-center align-middle">{{ $comment->id }}</td> --}}
                                    <td class="align-middle">{{ $comment->created_at->format("d/m/Y") }}</td>
                                    <td class="align-middle">{{ $comment->name }}</td>
                                    {{-- <td class="align-middle">{{ $comment->email }}</td> --}}
                                    <td class="align-middle">{{ $comment->pubtitle() }}</td>
                                    <td class="align-middle">{{ $comment->status }}</td>

                                    <td class="align-middle text-center">
                                        {{-- ver comentário --}}
                                        <a class="btn btn-sm btn-outline-primary mb-1"
                                            href="{{route('comments.show',$comment)}}" title="Visualizar"><i
                                                class="fas fa-eye"></i></a>


                                        {{-- Apagar comentário --}}
                                        <form method="POST" role="form" id="{{$comment->id}}" action="{{route('comments.destroy',$comment)}}"
                                        class="d-inline-block form_comment_id">
                                            @csrf
                                            @method('DELETE')

                                            <button type="submit" class="btn btn-sm btn-outline-danger btn-delete mb-1"
                                                title="Eliminar" data-toggle="modal" data-target="#deleteModal"><i class="fas fa-trash-alt"></i>
                                            </button>
                                        </form>

                                    </td>
                                </tr>
                                @endforeach
                                @endif
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section("scripts")
{{-- NAO APAGES SFFF --}}
<script src="{{asset('/js/script-comments.js')}}"></script>
@endsection
