<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="font-weight: 500">Eliminar Comentário</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <span>Tem a certeza que deseja eliminar este comentário?</span><br><br>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger btn-sm btn_submit"><i class="far fa-trash-alt mr-2"></i>Sim, eliminar comentário
                </button>
                <button type="button" class="btn btn-secondary btn-sm mr-2" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
