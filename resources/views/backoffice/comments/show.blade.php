@extends('backoffice.master-back')

@section('title', 'Comentários')

@section('content')



    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-7 align-self-center">
                <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Comentários do Blog</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb m-0 p-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}" class="text-dark">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{route('comments.index')}}" class="text-dark">Comentários
                                    do Blog</a></li>
                            <li class="breadcrumb-item text-muted active" aria-current="page">Comentário</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-5 text-right ">
                <a class="btn btn-md btn-primary shadow-sm" href="{{route('comments.index')}}"><i
                        class="fas fa-chevron-left text-white-50 mr-2"></i> Voltar à Listagem</a>
            </div>
        </div>
    </div>




    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <div class="row mb-3">
                            <div class="col">
                                <p><strong class="mr-2">Comentário no artigo:</strong> {{$posttitle->title}} <a
                                        class="ml-2"
                                        href="{{route('blogDetail', $comment->id_post)}}" target="_blank"
                                        title="Ver publicação"><i class="fas fa-external-link-alt"></i></a></p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="row">
                                    <div class="col col-6">
                                        <p><strong>Estado: </strong>
                                            @if ($comment->status=="Pendente")
                                                <span class="text-warning">Pendente</span>
                                            @else
                                                <span class="text-success">Aprovado</span>
                                            @endif
                                        </p>
                                    </div>
                                    <div class="col col-6 ">
                                        <strong>Data do comentário:</strong> {{$comment->created_at->format("d/m/Y")}}
                                    </div>
                                </div>


                                <br>
                                <div class="row">
                                    <div class="col col-6">
                                        <strong>Remetente:</strong> {{$comment->name}}
                                    </div>
                                    <div class="col col-6 ">
                                        <strong>E-mail:</strong> {{$comment->email}}
                                    </div>
                                </div>

                            </div>
                        </div>
                        <hr>
                        <h3>Comentário:</h3>
                        <p class="mt-2 p-2" style="background-color:#F4F8FB">{{$comment->comment}}</p>
                        <div class="d-flex justify-content-end">

                            {{-- Verifica se ja foi aprovado ou não --}}
                            @if($comment->status=="Pendente")
                                {{-- Aprovar comentário --}}
                                <form method="POST" role="form" action="{{route('comments.update',$comment)}}"
                                      class="d-inline-block form_user_id">
                                    @csrf
                                    @method("PUT")

                                    <button type="submit" class="btn btn-success text-white mr-3"
                                            title="Aprovar Comentário"><i class="fas fa-check text-white-50 mr-2"></i>Aprovar
                                        comentário
                                    </button>
                                </form>
                            @endif


                            {{-- Apagar comentário --}}
                            <form method="POST" role="form" action="{{route('comments.destroy',$comment)}}"
                                  class="d-inline-block form_user_id">
                            @csrf
                            @method('DELETE')
                            @include('backoffice.comments.partials.modal')
                            <!-- MODAL DE INFORMAÇÔES -->
                                <button type="button" class="btn btn-md btn-danger text-white shadow-sm"
                                        title="Eliminar Comentário"
                                        data-toggle="modal" data-target="#deleteModal"><i
                                        class="fas fa-times text-white-50 mr-2 "></i>Eliminar Comentário
                                </button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@section("scripts")


@endsection
