<!DOCTYPE html>
<html lang="pt" dir="ltr">

<head>
    <meta charset="utf-8">
    <!-- Tell the browser to be responsive to screen width -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title> @yield('title') | Mexico Estudar Portugal</title>

    <meta name="keywords" content="Estudar, Portugal, Universidade, Estudar Portugal, México">
    <meta name="description" content="">

    <!-- Favicon icon -->
    <link rel="shortcut icon" type="image/png" href="/media/favicon.png">

    <!-- Custom CSS -->
    <link href="{{asset('/vendor/extra-libs/c3/c3.min.css')}}" rel="stylesheet">
    <link href="{{asset('/vendor/chartist/dist/chartist.min.css')}}" rel="stylesheet">
    <link href="{{asset('/vendor/extra-libs/jvector/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet">
    <link href="{{asset('/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/vendor/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{asset('/css/style.min.css')}}" rel="stylesheet">
    <link href="{{asset('/css/style-loading.css')}}" rel="stylesheet">
    <link href="{{asset('css/style-backoffice-master.css')}}" rel="stylesheet">

    @yield('styleLinks')
</head>

<body onload="loading()">
<!-- Loader -->
<div id="loaderBg">
    <div id="loader"></div>
</div>

<div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
     data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">
    @include('backoffice.partials.topbar')
    @include('backoffice.partials.sidebar')

    <div class="page-wrapper">
        @if ($errors->any())
            @include ('backoffice.msg-error-message.partials.errors')
        @endif
        @if (!empty(session('success')))
            @include ('backoffice.msg-error-message.partials.success')
        @endif

        @yield('content')

        @include('backoffice.partials.footer')
    </div>
</div>

<script src="{{asset('/vendor/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('/vendor/popper.js/dist/umd/popper.min.js')}}"></script>
<script src="{{asset('/vendor/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- apps -->
<!-- apps -->
<script src="{{asset('/js/app-style-switcher.js')}}"></script>
<script src="{{asset('/js/feather.min.js')}}"></script>
<script src="{{asset('/vendor/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
<script src="{{asset('/js/sidebarmenu.js')}}"></script>
<!--Custom JavaScript -->
<script src="{{asset('/js/custom.min.js')}}"></script>
{{-- <script src="{{asset('/js/pages/datatable/datatable-basic.init.js')}}"></script> --}}
<!--This page JavaScript -->
<script src="{{asset('/vendor/extra-libs/c3/d3.min.js')}}"></script>
<script src="{{asset('/vendor/extra-libs/c3/c3.min.js')}}"></script>
<script src="{{asset('/vendor/chartist/dist/chartist.min.js')}}"></script>
<script src="{{asset('/vendor/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js')}}"></script>
<script src="{{asset('/vendor/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js')}}"></script>
<script src="{{asset('/vendor/extra-libs/jvector/jquery-jvectormap-world-mill-en.js')}}"></script>

<script src="{{asset('/vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

@yield('scripts') {{-- Scripts individuais --}}

<script>
    var loader = document.getElementById('loader');
    var loaderBg = document.getElementById('loaderBg');

    function loading() {
        loader.style.opacity = "0";
        loaderBg.style.opacity = "0";
        loaderBg.style.zIndex = "-1";
    }
</script>
</body>

</html>
