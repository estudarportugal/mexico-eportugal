@extends('backoffice.master-back')

@section('title', 'Dashboard')

@section('styleLinks')
<link href="{{asset('css/style-backoffice-dashboard.css')}}" rel="stylesheet">
@endsection

@section('content')

<div class="container-fluid">

    <h3 class="page-title text-truncate text-dark font-weight-medium mb-1">Dashboard</h3><br>


    <div class="row ">

        {{-- Visitas website --}}
        <div class="col text-center">
            <div class="card bg-success text-white"
                style=" outline: 2px solid #fff; outline-offset: -5px; height:200px; overflow:hidden;">
                <div class="card-body">
                    <span style="font-size:45px"><i class="ml-1 mt-2 fas fa-globe text-white"></i></span><br>
                    <h3>{{$visitasweb->quant}}</h3>
                    <h4>Visitas ao Website</h4>
                </div>
            </div>
        </div>


        {{-- Visitas LandingPage --}}
        <div class="col text-center">
            <div class="card bg-danger text-white"
                style=" outline: 2px solid #fff; outline-offset: -5px; height:200px; overflow:hidden;">
                <div class="card-body">
                    <span style="font-size:45px"><i class="ml-1 mt-2 far fa-file"></i></span><br>
                    <h3>{{$visitaslanding->quant}}</h3>
                    <h4>Visitas à Landing Page</h4>
                </div>
            </div>
        </div>

        {{-- Blog --}}
        <div class="col text-center">
            <div class="card bg-warning text-white"
                style=" outline: 2px solid #fff; outline-offset: -5px; height:200px; overflow:hidden;">
                <div class="card-body">
                    <span style="font-size:45px"><i class=" mt-2 far fa-newspaper"></i></span><br>
                    <h3>{{$totalpubs->quant}}</h3>
                    <h4>Publicações</h4>
                    <a href="{{route('posts.create')}}" class="newpost rounded p-1 bg-white"><span>
                            <small class="mr-2 ml-2"><i class="fas fa-plus mr-2"></i>Nova publicação</small>
                        </span></a>
                </div>
            </div>
        </div>

        {{-- Comentários--}}
        <div class="col text-center">
            <div class="card bg-primary text-white"
                style=" outline: 2px solid #fff; outline-offset: -5px; height:200px; overflow:hidden;">
                <div class="card-body">

                    <span style="font-size:45px"><i class="mt-2 far fa-comments"></i></span><br>
                    <h3>{{$totalcomments->quant}}</h3>
                    <h4>Comentários</h4>
                    @if ($penddingcomments->quant ==0)
                    <a href="{{route('comments.index')}}" class="stretched-link rounded p-1 bg-white">
                        <small class=" mr-2 ml-2 text-secondary">Nada pendente</small>
                    </a>
                    @else
                    <a href="{{route('comments.index')}}" class="stretched-link rounded p-1 bg-white"><span>
                            <small class=" mr-2 ml-2"><strong>{{$penddingcomments->quant}} por aprovar</strong></small>
                        </span></a>
                    @endif

                </div>
            </div>
        </div>
    </div>




    <div class="card text-center clock_back text-black">
        <div class="row py-2 m-1">

            <div class="col p-3 clock">
                <h3 id="horas1">00:00</h3><small>( UTC-03:00 )</small>
                <div>Argentina, Brasil, Chile, Malvinas, Paraguai</div>
            </div>


            <div class="col p-3 clock" >
                <h3 id="horas2">00:00</h3><small>( UTC-04:00 )</small>
                <div>Bolivia, Guyana, Venezuela</div>
            </div>

            <div class="col p-3 clock">
                <h3 id="horas3">00:00</h3><small>( UTC-05:00 )</small>
                <div>Colombia, Cuba, Equador, Peru, Panamá</div>
            </div>

            <div class="col p-3 clock">
                <h3 id="horas4">00:00</h3><small>( UTC-06:00 )</small>
                <div>Costa Rica, México</div>
            </div>


        </div>
    </div>



    <div class="row m-1">
        <div class="col card shadow-sm p-3">
            <h5 class="card-title">Origem das visitas <small class="text-muted">(total de {{$totalvisitas->quant}}
                    visitas)</small></h5>
            <hr>


            @if ($totalvisitas->quant==0)
            <span>Sem dados para mostrar</span>
            @endif

            @foreach ($geovisits as $geovisit)

            <div class="row mt-1 full_res">
                <div class="col col-2" style="min-width:100px">{{$geovisit->local}}: </div>
                <div class="col mr-2">
                    <?php
                             $calc=($geovisit->quant*100) / $totalvisitas->quant;
                              $calc=round($calc,1);

                            ?>
                    <div class="progress" style="height:20px">
                        <div class="progress-bar" role="progressbar" style="width: {{$calc}}%" aria-valuenow="25"
                            aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
                <div class="col col-2" style="min-width:210px">
                    {{$calc}} % ( {{$geovisit->quant}} visitas )
                </div>
            </div>

            <div class="row mobile_res mt-2">
                <div class="col">
                    <div class="progress" style="height:25px">
                        <div class="progress-bar" role="progressbar" style="width: {{$calc}}%;color:black"
                            aria-valuenow="{{$calc}}" aria-valuemin="0" aria-valuemax="100">
                            {{$geovisit->local}}:{{$calc}} % ( {{$geovisit->quant}} visitas )</div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>



</div>

@endsection




@section("scripts")
<script src="{{asset('/js/script-dashboard.js')}}"></script>
@endsection
