@extends('backoffice.master-back')

@section('title', 'Ajuda')

@section('content')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Área de Ajuda</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{route('home')}}" class="text-dark">Dashboard</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Ajuda</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div id="accordion">
                        {{-- AJUDA -> Criar uma publicação --}}
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                        Criar uma publicação
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <p>A <b>criação de uma nova publicação</b> deverá ser feita de acordo com os seguintes passos:</p>
                                    <p>- Escolher a opção "<i data-feather="file-text" class="feather-icon" style="width: 20px; height: 20px;"></i> <b>Publicações</b>" através do menu de navegação lateral</p>
                                    <p>- Pressionar o botão "<i class="fas fa-plus mr-1"></i> <b>Adicionar Publicação</b>"</p>
                                    <p>- Preencher os campos obrigatórios, assinalados com asterisco &#10033;</p>
                                    <p>- Para publicar a divulgação, clique em "<i class="fas fa-share-alt"></i> <b>Publicar</b>"<br><br></p>
                                </div>
                            </div>
                        </div>
                        {{-- AJUDA -> Gerir os comentários --}}
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Gerir os comentários
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    <p>A <b>gestão dos comentários</b> pode ser feita da seguinte forma:</p>
                                    <p>- Escolher a opção "<b> <i data-feather="message-square" class="feather-icon" style="width: 20px; height: 20px;"></i> Comentários</b>" através do menu de navegação lateral</p>
                                    <p>- Clicar no botão "<i class="fas fa-eye"></i>" para visualizar todos os detalhes acerca do comentário</p>
                                    <p>- Pressionar no botão "<b> <i class="fas fa-check"></i> Aprovar comentário</b>" caso pretenda aceitar o comentário</p>
                                    <p>- Caso pretenda eliminar o comentário, basta clicar no botão "<b> <i class="fas fa-times"></i> Eliminar comentário</b>"<br><br></p>
                                </div>
                            </div>
                        </div>
                        {{-- AJUDA -> Adicionar testemunhos --}}
                        <div class="card">
                            <div class="card-header" id="headingFour">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        Adicionar testemunhos
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Para <b>adicionar um testemunho</b> é necessário proceder da seguinte forma:</p>
                                    <p>- Escolher a opção "<b><i data-feather="user" class="feather-icon" style="width: 20px; height: 20px;"></i> Testemunhos</b>" através do menu de navegação lateral</p>
                                    <p>- Pressionar o botão "<i class="fas fa-plus mr-1"></i> <b>Adicionar Testemunho</b>"</p>
                                    <p>- Preencher os campos necessários</p>
                                    <p>- Para criar o testemunho, basta clicar no botão "<b> <i class="fas fa-plus"></i> Adicionar Testemunho</b>"<br><br></p>
                                </div>
                            </div>
                        </div>

                        {{-- AJUDA -> Gerir os comentários --}}
                        <div class="card">
                            <div class="card-header" id="headingFive">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                        Adicionar um utilizador
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                                <div class="card-body">
                                    <p>A <b>adição de utilizadores</b> deve seguir a seguinte forma:</p>
                                    <p>- Escolher a opção "<b> <i data-feather="users" class="feather-icon" style="width: 20px; height: 20px;"></i> Utilizadores</b>" através do menu de navegação lateral</p>
                                    <p>- Pressionar o botão "<i class="fas fa-plus mr-1"></i> <b>Adicionar Utilizador</b>"</p>
                                    <p>- Preencher os campos necessários, e de seguida confirmar a adição</p><br><br>
                                    <p> <b>Nota:</b> <br>
                                        - Toda a gestão dos utilizadores apenas pode ser efectuada se estiver autenticado como <b>Administrador</b>.<br>
                                        - A <b>password</b> é escolhida pelo utilizador inserido, quando este receber um e-mail de confirmação de conta.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
