@extends('backoffice.master-back')

@section('title', 'Testemunhos')


@section('styleLinks')
<link href="{{asset('css/style-backoffice-testimonials.css')}}" rel="stylesheet">

@endsection


@section('content')

@include('backoffice.testimonials.partials.modal')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Listagem de Testemunhos</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{route('home')}}" class="text-dark">Dashboard</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Testemunhos</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 text-right ">
            <a href="{{route('testimonials.create')}}" class="btn btn-md btn-primary shadow-sm">
                <i class="fas fa-plus text-white-50 mr-2"></i> Adicionar Testemunho
            </a>
        </div>
    </div>
</div>

<div class="container-fluid">
<!-- Content Row -->
<div class="card shadow mb-4">

    <div class="card-body">


        <div class="table-responsive">

            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th class="text-center">Foto</th>
                        <th>Nome</th>
                        <th>Data</th>
                        <th class="text-center">Opções</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($testimonials as $testimonial)
                    <tr>

                        <!-- Foto  -->
                        <td class="align-middle text-center ">
                            @if ($testimonial->photo)
                            <img src="{{Storage::disk('public')->url('testimonials-photos/').$testimonial->photo}}" style="width:90px;" class="rounded-circle shadow-sm foto" alt="Imagem de apresentação">
                            @else
                            <img src="{{asset('img/default_user.png')}}" style="width:90px;" class="rounded-circle shadow-sm" alt="Imagem de apresentação">
                            @endif
                        </td>

                        <!-- Nome -->
                        <td class="align-middle">{{ $testimonial->name }}</td>


                        <!-- Data -->
                        <td class="align-middle">{{ $testimonial->created_at->format("d/m/Y")}}</td>


                        <!-- Opções -->
                        <td nowarp class="text-center align-middle">

                            <!-- Função View -->
                            <a class="btn btn-sm btn-outline-primary mb-1" href="{{ route('testimonials.show',$testimonial->id) }}" title="Ver testemunho"><i class="fas fa-eye"></i></a>


                            <!-- Função Edit -->
                            <a class="btn btn-sm btn-outline-warning mb-1" href="{{ route('testimonials.edit',$testimonial->id) }}" title="Editar testemunho"><i class="fas fa-pen"></i></a>

                            <!-- Função delete -->
                                <form method="POST" role="form" id="{{ $testimonial->id }}" action="{{route('testimonials.destroy',$testimonial)}}" class="d-inline-block form_testimonial_id" >
                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" class="btn btn-sm btn-outline-danger btn-delete mb-1" title="Eliminar testemunho" data-toggle="modal" data-target="#deleteModal" ><i class="fas fa-trash-alt"></i></button>

                                </form>

                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>



        </div>

</div>
        @endsection




        @section('scripts')

        <script src="{{asset('/js/script-testimonials.js')}}"></script>

        @endsection
