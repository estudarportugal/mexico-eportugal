<div class="row">

    <div class="col col-fields">
        <label for="nome">Nome: </label>
        <input type="text" class="p-2" name="name" id="name_txt" value="{{old('testimonial',$testimonial->name)}}"
               placeholder="Escreva aqui o nome" style="width:100%;">
        <span id="warning_name"
              class="warning warning_info_hidden mt-1">- O campo "Nome" é de preenchimento obrigatório</span>

        <br><br>

        <label for="description">Testemunho: </label>
        <textarea rows="5" class="p-2" name="description" id="description_txt" placeholder="Escreva aqui o comentário"
                  style="width:100%;resize: none;">{{old('testimonial',$testimonial->description)}}</textarea>
        <span id="warning_testemunho" class="warning warning_info_hidden mt-1">- O campo "Testemunho" é de preenchimento obrigatório</span>

    </div>


</div>

<div class="row mt-4">
    <div class="col">
        <label for="photo">Selecionar fotografia: </label><br>
        <div class="custom-file">
            <input type="file" class="custom-file-input" name="photo" id="inputImage" accept="image/*"
                   value="{{old('testimonial',$testimonial->photo)}}"  aria-describedby="fileHelp">
            <label class="custom-file-label" id="file" for="inputGroupFile01">Selecione a imagem</label>
            <br><span id="warning_img" class="warning warning_info_hidden mt-1">- O ficheiro da fotografia é obrigatória e não pode ser maior que 2mb</span>
        </div>
    </div>
</div>

{{--
<div class="row mt-4">
    <div class="col">
        <label for="photo">Selecionar fotografia: </label><br>
        <input type="file" name="photo" id="photo" class="file" accept="image/*" value="{{old('testimonial',$testimonial->photo)}}" aria-describedby="fileHelp">
        <br><span id="warning_img" class="warning warning_info_hidden mt-1">- O ficheiro da fotografia é obrigatória e não pode ser maior que 2mb</span>
    </div>
</div>
--}}
