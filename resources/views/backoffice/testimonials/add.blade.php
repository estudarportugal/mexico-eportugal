@extends('backoffice.master-back')

@section('title', 'Novo Testemunho')

@section('styleLinks')
<link href="{{asset('css/style-backoffice-testimonials.css')}}" rel="stylesheet">
@endsection


@section('content')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Novo testemunho</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{route('home')}}" class="text-dark">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('testimonials.index')}}" class="text-dark">Testemunhos</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Novo testemunho</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 text-right ">
            <a href="{{route('testimonials.index')}}" class="btn btn-md btn-primary shadow-sm">
                <i class="fas fa-chevron-left text-white-50 mr-2"></i> Voltar
            </a>
        </div>
    </div>
</div>


<div class="container-fluid">

<!-- Basic Card Example -->
<div class="card shadow ">


    <div class="card-body ">

        <form method="POST" action="{{route('testimonials.store')}}" class="form-group pt-3" id="testemunho_form" enctype="multipart/form-data">
            @csrf
            @include('backoffice.testimonials.partials.add-edit')
            <div class="form-group text-right">
                <br><br>
                <button type="button" class="btn btn-primary mr-2" name="ok" id="buttonSubmit"><i class="fas fa-plus text-white-50 mr-2"></i>Adicionar testemunho</button>
                <a href="{{route('testimonials.index')}}" class="btn btn-secondary text-white">Cancelar</a>
            </div> </form>

        </div>

    </div>
</div>
        @endsection

        @section('scripts')

        <script src="{{asset('/js/script-testimonials-add.js')}}"></script>


        @endsection
