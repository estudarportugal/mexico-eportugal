@extends('backoffice.master-back')

@section('title', 'Testemunho')

@section('content')



<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Testemunho de {{$testimonial->name}}
            </h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{route('home')}}" class="text-dark">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('testimonials.index')}}"
                                class="text-dark">Testemunhos</a></li>
                        <li class="breadcrumb-item">Testemunho de {{$testimonial->name}}</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 text-right ">
            <a href="{{route('testimonials.index')}}" class="btn btn-md btn-primary shadow-sm">
                <i class="fas fa-chevron-left text-white-50 mr-2"></i> Voltar
            </a>
            <a href="{{ route('testimonials.edit',$testimonial->id) }}"
                class="btn btn-md btn-warning text-white shadow-sm">
                <i class="fas fa-pen text-white-50 mr-2"></i> Editar
            </a>
        </div>
    </div>
</div>



<div class="container-fluid">
    <div class="card">
        <div class="row p-4">
            <div class="col">

                <div><strong>Nome:</strong> {{$testimonial->name}}</div>

                <br>
                <div><strong>Testemunho:</strong><br><p class="pt-2">{{$testimonial->description}}</p></div>

            </div>

            <div class="col text-center">
                <div class="mb-4"><strong>Fotografia</strong></div>

                    <img class="card mx-auto" style="width:220px;" src="{{Storage::disk('public')->url('testimonials-photos/').$testimonial->photo}}">

            </div>
        </div>

    </div>

</div>

@endsection
