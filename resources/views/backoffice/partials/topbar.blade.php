<header class="topbar" data-navbarbg="skin6">
    <nav class="navbar top-navbar navbar-expand-md">
        <div class="navbar-header" data-logobg="skin6">
            <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
            <div class="navbar-brand">
                <!-- Logo icon -->
                <a href="{{route('home')}}">
                    <b class="logo-icon">
                        <!-- Dark Logo icon -->
                        <img src="{{asset('/media/logo-icon.png')}}" alt="homepage" class="dark-logo" />
                        <!-- Light Logo icon -->
                        <img src="{{asset('/media/logo-icon.png')}}" alt="homepage" class="light-logo" />
                    </b>
                    <!--End Logo icon -->
                    <!-- Logo text -->
                    <span class="logo-text">
                        <!-- dark Logo text -->
                        <img src="{{asset('/media/logo-text.png')}}" alt="homepage" class="dark-logo" />
                        <!-- Light Logo text -->
                        <img src="{{asset('/media/logo-light-text.png')}}" class="light-logo" alt="homepage" />
                    </span>
                </a>
            </div>
            <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
              aria-label="Toggle navigation"><i class="ti-more"></i></a>
        </div>
        <div class="navbar-collapse collapse" id="navbarSupportedContent">
            <ul class="navbar-nav float-right mr-auto ml-5 pl-2">

            </ul>
            <ul class="navbar-nav float-right">
                <li class="nav-item dropdown float-right mr-auto ml-5 pl-3" style="color: #6d6d6d; font-size: 2px">
                    <a class="nav-link dropdown-toggle" href="http://mexico.eportugal.test/" target="_blank" title="Ver Website">
                        <i data-feather="monitor" class="svg-icon mb-1"></i>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="ml-2 d-none d-lg-inline-block"><span>Olá,</span> <span class="text-dark">{{Auth::user()->name}}</span> <i data-feather="chevron-down" class="svg-icon"></i></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                        <a class="dropdown-item" href="{{route('help')}}"><i data-feather="help-circle" class="svg-icon mr-2 ml-1"></i>
                            Ajuda</a>
                        <div class="dropdown-divider"></div>
                        <form action="{{route('logout')}}" method="post">
                            @csrf
                            <button class="dropdown-item" type="submit"><i data-feather="power" class="svg-icon mr-2 ml-1"></i>Logout</button>
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>
