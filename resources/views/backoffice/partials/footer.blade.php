<footer class="footer text-muted">
    <p style="font-size: 14px; margin-bottom: 5px; margin-top: 5px; text-align: center">&copy;
        <script>
            document.write(new Date().getFullYear());
        </script>
        Copyright <a href="http://mexico.eportugal.test/" target="_blank">Estudar Portugal</a> Todos os direitos reservados.
    </p>
</footer>
