<aside class="left-sidebar" data-sidebarbg="skin6">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar" data-sidebarbg="skin6">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="sidebar-item">
                    <a class="sidebar-link sidebar-link" href="{{route('home')}}" aria-expanded="false">
                        <i data-feather="home" class="feather-icon"></i>
                        <span class="hide-menu">Dashboard</span>
                    </a>
                </li>

                <li class="list-divider"></li>

                <li class="nav-small-cap">
                    <span class="hide-menu">Gerir Conteúdos</span>
                </li>

                <li class="sidebar-item {{Route::is('posts.*')?'selected':''}}">
                    <a class="sidebar-link sidebar-link" href="{{route('posts.index')}}" aria-expanded="false">
                        <i data-feather="file-text" class="feather-icon"></i>
                        <span class="hide-menu">Publicações</span>
                    </a>
                </li>

                <li class="sidebar-item {{Route::is('comments.*')?'selected':''}}">
                    <a class="sidebar-link sidebar-link" href="{{route('comments.index')}}" aria-expanded="false">
                        <i data-feather="message-square" class="feather-icon"></i>
                        <span class="hide-menu">Comentários</span>
                    </a>
                </li>

                <li class="sidebar-item  {{Route::is('landingpages.*') ? 'selected' : ''}}">
                    <a class="sidebar-link sidebar-link" href="{{route('landingpages.edit', 1)}}" aria-expanded="false">
                        <i data-feather="file" class="feather-icon"></i>
                        <span class="hide-menu">Landing Page</span>
                    </a>
                </li>

                <li class="sidebar-item  {{Route::is('testimonials.*') ? 'selected' : ''}}">
                    <a class="sidebar-link sidebar-link" href="{{route('testimonials.index')}}" aria-expanded="false">
                        <i data-feather="user" class="feather-icon"></i>
                        <span class="hide-menu">Testemunhos</span>
                    </a>
                </li>

                <li class="list-divider"></li>

                @if (Auth::user()->role == "Admin")
                <li class="nav-small-cap">
                    <span class="hide-menu">Outras Ferramentas</span>
                </li>
                <li class="sidebar-item {{Route::is('users.*') ? 'selected' : ''}}">
                    <a class="sidebar-link sidebar-link" href="{{route('users.index')}}" aria-expanded="false">
                        <i data-feather="users" class="feather-icon"></i>
                        <span class="hide-menu">Utilizadores</span>
                    </a>
                </li>
                @endif
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
