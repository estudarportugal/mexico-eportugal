@extends('backoffice.master-back')

@section('title', 'Publicações')

@section('content')
    @include('backoffice.posts.partials.modal')
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-7 align-self-center">
                <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Listagem de Publicações</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb m-0 p-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}" class="text-dark">Dashboard</a></li>
                            <li class="breadcrumb-item text-muted active" aria-current="page">Publicações</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-5 text-right ">
                <a href="{{route('posts.create')}}" class="btn btn-md btn-primary shadow-sm">
                    <i class="fas fa-plus text-white-50 mr-2"></i> Adicionar Publicação
                </a>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Título</th>
                                    <th>Texto Curto</th>
                                    <th style="width: 20%">Data de Criação</th>
                                    <th style="width: 120px; text-align: center">Opções</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($posts as $post)
                                    <tr>
                                        <td>{{$post->title}}</td>
                                        <td>{{$post->resume}}</td>
                                        <td><?=date('d/m/Y', strtotime($post->created_at))?></td>
                                        <td id="test" class="text-center">
                                            <a class="btn btn-sm btn-outline-primary rounded"
                                               href="{{route('posts.show',$post)}}"
                                               title='Visualizar'>
                                                <i class="fas fa-eye"></i>
                                            </a>

                                            <a class="btn btn-sm btn-outline-warning Editar rounded"
                                               href="{{route('posts.edit',$post)}}"
                                               title='Editar'>
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>

                                            <form method="POST" action="{{route('posts.destroy',$post)}}" role="form"
                                                  class="d-inline-block form_post_id" title='Eliminar'>
                                                @csrf
                                                @method("DELETE")
                                                <button type="button" class="btn btn-sm btn-outline-danger rounded"
                                                        data-toggle="modal" data-target="#eliminarPublicacao"
                                                        data-title="{{$post->title}}">
                                                    <i class="fas fa-trash-alt"></i>
                                                </button>
                                            </form>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("scripts")
    <script>
        $('#dataTable').DataTable({
            "columnDefs": [
                {"orderable": false, "targets": 3},

                {"width": "120px", "targets": 0},

                {"width": "130px", "targets": 2},
                {"width": "120px", "targets": 3}

            ],
            "order": [[0, "desc"]],

            "language": {'url': '//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese.json'}
        });

        $(document).ready(function () {
            $('#eliminarPublicacao').on('show.bs.modal', function (e) {
                $("#eliminar").click(function () {
                    $(e.relatedTarget).parent().submit();
                });
                $("#titulo").text($(e.relatedTarget).data("title"));
            });
        });

        $('#eliminarPublicacao').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var recipient = button.data('whatever');
            var modal = $(this);
            modal.find('.modal-body input').val(recipient);
        });
    </script>
@endsection

