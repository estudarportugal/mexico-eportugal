@extends('backoffice.master-back')

@section('styleLinks')
    <link rel="stylesheet" type="text/css" href="{{asset('/css/style-backoffice-posts.css')}}">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/bootstrap-tokenfield.min.css">
@endsection

@section('title', 'Editar Publicação')

@section('content')
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-7 align-self-center">
                <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Editar Publicação</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb m-0 p-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}" class="text-dark">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{route('posts.index')}}"
                                                           class="text-dark">Publicações</a></li>
                            <li class="breadcrumb-item text-muted active" aria-current="page">Editar Publicação</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-5 text-right">
                <a href="{{route('posts.index')}}" class="btn btn-md btn-primary shadow-sm">
                    <i class="fas fa-chevron-left text-white-50 mr-2"></i> Voltar à Listagem
                </a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="{{route('posts.update',$post)}}" class="form-group"
                              enctype="multipart/form-data">
                            @csrf
                            @method("PUT")
                            @include('backoffice.posts.partials.add-edit')
                            <div class="text-right">
                                <div class="form-group col-md-12 text-right mt-3 ml-3">
                                    <button type="submit" class="btn btn-md btn-success text-white mr-1" name="ok" title="Guardar Publicação"><i class="far fa-save mr-2"></i> Guardar
                                    </button>
                                    <a href="{{route('posts.index')}}" class="btn btn-md btn-secondary text-white mr-1 my-1" title="Cancelar Publicação">Cancelar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Contagem de carateres -->
    <script src="{{asset('/js/script-characters.js')}}"></script>

    <!--CKeditor - Ficheiros do CKeditor-->
    <script src="{{asset('/vendor/ckeditor/ckeditor.js')}}"></script>

    <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>

    <!-- Script Imagens -->
    <script src="{{asset('/js/script-posts-back.js')}}"></script>

    <script> CKEDITOR.replace('inputDescription');</script>

    <script>
        $(function () {
            $('input[type=text], textarea').keyup();
        });
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.js"></script>

    <script>
        $(document).ready(function(){
            $('#inputTags').tokenfield({
                autocomplete:{
                    delay:100
                },
                showAutocompleteOnFocus: true
            });
            $('#inputTags_div').on('submit', function(event){
                event.preventDefault();
                if($.trim($('#inputTags').val()).length == 0)
                {
                    alert("Please Enter Atleast one Skill");
                    return false;
                }
            });
        });
    </script>
@endsection
