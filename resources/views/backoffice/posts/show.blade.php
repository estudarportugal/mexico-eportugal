@extends('backoffice.master-back')

@section('title', 'Detalhe da Publicação')

@section('content')
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-7 align-self-center">
                <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Visualização da Divulgação</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb m-0 p-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}" class="text-dark">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{route('posts.index')}}"
                                                           class="text-dark">Publicações</a></li>
                            <li class="breadcrumb-item text-muted active" aria-current="page">Editar Publicação</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-5 text-right">
                <a href="{{route('posts.index')}}" class="btn btn-md btn-primary shadow-sm">
                    <i class="fas fa-chevron-left text-white-50 mr-2"></i> Voltar à Listagem
                </a>
                <a href="{{route('posts.edit',$post)}}" class="btn btn-md btn-warning shadow-sm" style="color: white">
                    <i class="fas fa-pen text-white-50 mr-2"></i> Editar Divulgação
                </a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <div class="row">
                            <div class="form-group col-md-6 col-sm-12">
                                <div class="col-sm">
                                    <label class="text-dark" style="font-weight:500;">Título:</label> {{$post->title}}
                                </div>
                            </div>

                            <div class="form-group col-md-6 col-sm-12">
                                <div class="col-md">
                                    <label class="text-dark" style="font-weight: 500">Data de Publicação: </label>
                                    {{$post->created_at->format('d/m/Y')}}
                                </div>
                            </div>

                            <div class="form-group col-md-6 col-sm-12">
                                <div class="col-sm">
                                    <label class="text-dark" style="font-weight: 500">Texto
                                        Curto: </label> {{$post->resume}}
                                </div>
                            </div>

                            @if($post->location === null)
                            @else
                                <div class="form-group col-md-6 col-sm-12">
                                    <div class="col-sm">
                                        <label class="text-dark"
                                               style="font-weight: 500">Localização: </label> {{$post->location}}
                                    </div>
                                </div>
                            @endif

                            @if($post->tags === null)
                            @else
                                <div class="form-group col-12">
                                    <div class="col-md">
                                        <label class="text-dark" style="font-weight: 500">Tags: </label> {{$post->tags}}
                                    </div>
                                </div>
                            @endif

                            <div class="form-group col-md-12">
                                <div class="col-md">
                                    <label class="text-dark" style="font-weight: 500">Descrição da
                                        Publicação: </label>{!! $post->description !!}
                                </div>
                            </div>

                            <div class="form-group col-md-12 ml-3">
                                @if ($post->image === null)
                                @else
                                    <img alt="Imagem da divulgação" style="height: 100px!important; width:auto"
                                         src="{{Storage::disk('public')->url('posts-images/').$post->image}}">
                                @endif
                            </div>

                            <div class="form-group col-md-12 ml-3">
                                @if ($post->video)
                                    <video width="auto" height="200" controls>
                                        <source src="{{Storage::disk('public')->url('posts-videos/').$post->video}}"
                                                type="video/mp4">
                                    </video>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
