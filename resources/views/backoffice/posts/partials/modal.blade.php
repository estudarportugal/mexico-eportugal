<div class="modal fade" id="eliminarPublicacao" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="font-weight: 500">Eliminar Publicação</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Tem a certeza que pretende eliminar a publicação "<span style="font-weight: 500" id="titulo"></span>"?
                </p>
            </div>
            <div class="modal-footer">
                <button id="eliminar" type="button" class="btn btn-danger btn-sm">
                    <i class="far fa-trash-alt mr-2"></i>Eliminar
                </button>
                <button type="button" class="btn btn-secondary btn-sm mr-2" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
