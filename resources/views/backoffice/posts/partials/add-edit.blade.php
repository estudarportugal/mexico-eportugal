<div class="row">
    <div class="form-group col-lg-12 col-md-6">
        <label for="inputTitle"><span class="campo_obrigatorio text-danger small">&#10033;</span>Título</label>
        <input type="text" class="form-control" name="title" id="inputTitle" maxlength="150"
               value="{{old('title',$post->title)}}"/>
        <h6 class="pull-right text-right small mt-2" id="count_message1"></h6>
    </div>

    <div class="form-group col-12 col-md-6">
        <span class="campo_obrigatorio text-danger small">&#10033;</span><label
            for="inputResume">Texto Curto <small>(Sobre a Publicação)</small></label>
        <textarea class="form-control" name="resume" id="inputResume" rows="2" style="resize: none" maxlength="150"
                  fixed>{{old('resume',$post->resume)}}</textarea>
        <h6 class="pull-right text-right small mt-2" id="count_message3"></h6>
    </div>

    <div class="form-group col-12 col-md-6">
        <label for="inputLocation">Local</label>
        <input type="text" class="form-control" name="location" id="inputLocation"
               maxlength="150" value="{{old('location',$post->location)}}" >
        <h6 class="pull-right text-right small mt-2" id="count_message4"></h6>
    </div>

    <div class="form-group col-md-12 col-sm-12" id="tagsForm">
        <label for="inputTags">Tags <small>(Clique em 'enter' para obter a tag)</small></label>
        <input type="text" class="form-control" name="tags" id="inputTags" value="{{old('tags',$post->tags)}}">
    </div>

    <div class="form-group col-lg-6 col-md-12 mt-4">
        <div class="input-group-prepend">
            <label for="inputImage">Imagem <small id="fileHelp">(O tamanho da Imagem não deve ultrapassar os 2MB)</small></label>
        </div>
        <div class="custom-file">
            <input type="file" class="custom-file-input" name="image" id="inputImage" accept="image/*" aria-describedby="fileHelp">
            <label class="custom-file-label" id="file" for="inputGroupFile01">Selecione a imagem</label>
        </div>
    </div>

    <div class="form-group col-lg-6 col-md-12 mt-4">
        <div class="input-group-prepend">
            <label for="inputVideo">Vídeo <small id="fileHelpVideo">(O tamanho do Vídeo não deve ultrapassar os 20MB)</small></label>
        </div>
        <div class="custom-file">
            <input type="file" class="custom-file-input" name="video" id="inputVideo" accept="video/*" aria-describedby="fileHelpVideo">
            <label class="custom-file-label" id="fileVideo" for="inputGroupFile02">Selecione o Vídeo</label>
        </div>
    </div>

    <div class="form-group col-md-12">
        <br>
        <span class="campo_obrigatorio text-danger small">&#10033;</span><label for="inputDescription">Descrição</label>
        <textarea name="description" id="inputDescription" required>{{old('description',$post->description)}}</textarea>
        <br>
        <small class="campo_obrigatorio text-danger">&#10033; Campo de preenchimento obrigatório</small>
    </div>
</div>
