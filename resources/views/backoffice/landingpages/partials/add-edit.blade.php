<div class="row">
    <div class="form-group col-lg-12 col-md-6">
        <label for="inputTitleLanding"><span class="campo_obrigatorio text-danger small">&#10033;</span>Título</label>
        <input type="text" class="form-control" name="title" id="inputTitleLanding" maxlength="150"
               value="{{old('title',$landingpage->title)}}"/>
        <h6 class="pull-right text-right small mt-2" id="count_message5"></h6>
    </div>

    <div class="form-group col-lg-7 col-md-7 col-sm-7">
        <label for="inputDescriptionLanding"><span
                class="campo_obrigatorio text-danger small">&#10033;</span>Descrição<small> (Sobre a Landing Page)</small></label>
        <input type="text" class="form-control" name="description" id="inputDescriptionLanding" maxlength="150"
               value="{{old('description',$landingpage->description)}}"/>
        <h6 class="pull-right text-right small mt-2" id="count_message6"></h6>
    </div>

    <div class="form-group col-lg-5 col-md-5 col-sm-5" id="tagsFormLanding">
        <label for="inputTagsLanding">Tags</label>
        <input type="text" class="form-control" name="tags" id="inputTagsLanding"
               value="{{old('tags',$landingpage->tags)}}">
    </div>

    <div class="form-group col-md-6 col-sm-6" id="contact1Form">
        <label for="inputContact1">Contacto 1</label>
        <input type="text" class="form-control" name="contact1" id="inputContact1"
               value="{{old('contact1',$landingpage->contact1)}}">
    </div>

    <div class="form-group col-md-6 col-sm-6" id="contact2Form">
        <label for="inputContact2">Contacto 2</label>
        <input type="text" class="form-control" name="contact2" id="inputContact2"
               value="{{old('contact2',$landingpage->contact2)}}">
    </div>

    <div class="form-group col-lg-6 col-md-12 mt-4">
        <div class="input-group-prepend">
            <label for="inputImage1">Imagem de Fundo1 <small id="fileHelp1">(O tamanho da Imagem não deve ultrapassar os 2MB)</small></label>
        </div>
        <div class="custom-file">
            <input type="file" class="custom-file-input" name="backgroundimage1" id="inputImage1" accept="image/*" aria-describedby="fileHelp1">
            <label class="custom-file-label" id="file1" for="inputGroupFile01">Selecione a imagem</label>
        </div>
    </div>

    <div class="form-group col-lg-6 col-md-12 mt-4">
        <div class="input-group-prepend">
            <label for="inputImage2">Imagem de Fundo2 <small id="fileHelp2">(O tamanho da Imagem não deve ultrapassar os 2MB)</small></label>
        </div>
        <div class="custom-file">
            <input type="file" class="custom-file-input" name="backgroundimage2" id="inputImage2" accept="image/*" aria-describedby="fileHelp2">
            <label class="custom-file-label" id="file2" for="inputGroupFile02">Selecione a imagem</label>
        </div>
    </div>
</div>
