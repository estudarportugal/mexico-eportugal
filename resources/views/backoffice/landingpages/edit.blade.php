@extends('backoffice.master-back')

@section('styleLinks')
    <link rel="stylesheet" type="text/css" href="{{asset('/css/style-backoffice-posts.css')}}">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/bootstrap-tokenfield.min.css">
@endsection

@section('title', 'Editar Landing Page')

@section('content')
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-7 align-self-center">
                <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Editar Landing Page</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb m-0 p-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}" class="text-dark">Dashboard</a></li>
                            <li class="breadcrumb-item text-muted active" aria-current="page">Editar Landing Page</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="{{route('landingpages.update', 1)}}" class="form-group"
                              enctype="multipart/form-data">
                            @csrf
                            @method("PUT")
                            @include('backoffice.landingpages.partials.add-edit')
                            <div class="text-right">
                                <div class="form-group col-md-12 text-right mt-3 ml-3">
                                    <button type="submit" class="btn btn-md btn-success text-white mr-1" name="ok" title="Guardar Landing Page"><i class="far fa-save mr-2"></i> Guardar
                                    </button>
                                    <button class="btn btn-md btn-primary text-white mr-1 my-1" title="Visualizar Landing Page">
                                        <i class="far fa-eye mr-2"></i> <a  style="text-decoration: none; color: white" href="{{route('landingpages.index', 1)}}" target="_blank"> Visualizar </a>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Contagem de carateres -->
    <script src="{{asset('/js/script-characters.js')}}"></script>

    <!-- Script Imagens -->
    <script src="{{asset('/js/script-landing.js')}}"></script>

    <script>
        $(function () {
            $('input[type=text], textarea').keyup();
        });
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.js"></script>

    <script>
        $(document).ready(function () {
            $('#inputTagsLanding').tokenfield({
                autocomplete: {
                    delay: 100
                },
                showAutocompleteOnFocus: true
            });
            $('#inputTagsLanding_div').on('submit', function (event) {
                event.preventDefault();
                if ($.trim($('#inputTagsLanding').val()).length == 0) {
                    alert("Please Enter Atleast one Skill");
                    return false;
                }
            });
        });
    </script>
@endsection
