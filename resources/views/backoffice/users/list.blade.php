@extends('backoffice.master-back')

@section('title', 'Utilizadores')

@section('styleLinks')
<link rel="stylesheet" href="{{asset('/css/style-users.css')}}">
@endsection

@section('content')
@include('backoffice.users.partials.modal')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Listagem de Utilizadores</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{route('home')}}" class="text-dark">Dashboard</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Utilizadores</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 text-right ">
            <a href="{{route('users.create')}}" class="btn btn-md btn-primary shadow-sm">
                <i class="fas fa-plus text-white-50 mr-2"></i> Adicionar Utilizador
            </a>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @if (count($users))
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Endereço Eletrónico</th>
                                    <th>Função</th>
                                    <th style="width:120px; text-align: center;">Opções</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>
                                        @if ($user->role == "Admin")Administrador
                                        @else Funcionário
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-sm btn-outline-primary rounded" href="{{route('users.show', $user)}}" title='Visualizar'>
                                            <i class="fas fa-eye"></i>
                                        </a>

                                        <a class="btn btn-sm btn-outline-warning rounded" href="{{route('users.edit', $user)}}" title='Editar'>
                                            <i class="fas fa-pencil-alt"></i>
                                        </a>

                                        @if (Auth::user()->id == $user->id)
                                        <a class="btn btn-sm btn-outline-danger rounded disabled" href="">
                                            <i class="fas fa-trash-alt"></i>
                                        </a>
                                        @else
                                        <form method="POST" action="{{route('users.destroy',$user)}}" role="form" class="d-inline-block form_post_id" title='Eliminar'>
                                            @csrf
                                            @method("DELETE")
                                            <button type="button" class="btn btn-sm btn-outline-danger rounded" data-toggle="modal" data-target="#eliminarPublicacao" data-title="{{$user->name}}" data-id="{{$user->id}}">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @else
                    <h5 style="text-align: center">Não existem utilizadores registados.</h5>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@section("scripts")
<script>
    $(document).ready(function() {
        $('#dataTable').DataTable({
            "columnDefs": [{
                    "orderable": false,
                    "targets": 3
                },

                {
                    "width": "120px",
                    "targets": 0
                },

                {
                    "width": "130px",
                    "targets": 2
                },
                {
                    "width": "120px",
                    "targets": 3
                }

            ],
            "order": [
                [2, "asc"]
            ],

            "language": {
                'url': '//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese.json'
            }
        });

    });
    $(document).ready(function() {
        $('#eliminarPublicacao').on('show.bs.modal', function(e) {
            $("#eliminar").click(function() {
                $(e.relatedTarget).parent().submit();
            });
            $("#titulo").text($(e.relatedTarget).data("title"));
        });
    });

    $('#eliminarPublicacao').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget);
        var recipient = button.data('whatever');
        var modal = $(this);
        modal.find('.modal-body input').val(recipient);
    });
</script>
@endsection

@endsection
