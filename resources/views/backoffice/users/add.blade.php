@extends('backoffice.master-back')

@section('title', 'Utilizadores')

@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Criação Utilizador</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{route('home')}}" class="text-dark">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('users.index')}}" class="text-dark">Utilizadores</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Adicionar Utilizador</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 text-right ">
            <a href="javascript:history.go(-1)" class="btn btn-md btn-primary shadow-sm">
                <i class="fas fa-chevron-left text-white-50 mr-2"></i>
                Voltar à Listagem
            </a>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h3>Adicionar Utilizador</h3><br>
                    <form action="{{route('users.store')}}" method="post" class="form-group" enctype="multipart/form-data">
                        @csrf
                        @include('backoffice.users.partials.add-edit')
                        <div class="text-right">
                            <div class="form-group col-md-12 text-right mt-3 ml-3">
                                <button type="submit" class="btn btn-md btn-primary text-white mr-1" name="confirm" title='Editar Utilizador'>
                                    Adicionar
                                </button>
                                <a href="{{route('users.index')}}" class="btn btn-md btn-secondary text-white mr-1 my-1" name="btn_cancelar" id="btn_cancelar" title='Cancelar Edição'>Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
