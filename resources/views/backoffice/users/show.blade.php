@extends('backoffice.master-back')

@section('title', 'Utilizadores')

@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Visualização do Utilizador</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{route('home')}}" class="text-dark">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('users.index')}}" class="text-dark">Utilizadores</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Visualização Detalhada</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 text-right ">
            <a href="javascript:history.go(-1)" class="btn btn-md btn-primary shadow-sm">
                <i class="fas fa-chevron-left text-white-50 mr-2"></i>
                Voltar à Listagem
            </a>
            <a href="{{route('users.edit', $user)}}" class="btn btn-md btn-warning shadow-sm" style="color:white;">
                <i class="fas fa-pencil-alt text-white-50 mr-2"></i> Editar Utilizador
            </a>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h3><b>{{$user->name}}</b></h3><br>
                    <h5 style="line-height: 1.6"><b>Email: </b>{{$user->email}}</h5>
                    <h5 style="line-height: 1.6"><b>Função: </b>@if($user->role == "Admin")Administrador @else Funcionário @endif</h5>
                    <h5 style="line-height: 1.6"><b>Data de criação: </b>{{date('d/m/Y', strtotime($user->created_at))}}</h5>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
