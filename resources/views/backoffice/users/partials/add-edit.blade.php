<div class="row">
    <div class="form-group col-lg-12 col-md-6">
        <label for="inputName"><span class="campo_obrigatorio text-danger small">&#10033;&nbsp;</span>Nome</label>
        <input type="text" class="form-control" name="name" id="inputName" maxlength="150" value="{{old('name',$user->name)}}" />
    </div>
</div>
<br>
<div class="row">
    <div class="form-group col-12 col-md-6">
        <label for="inputEmail"><span class="campo_obrigatorio text-danger small">&#10033;&nbsp;</span>Endereço Eletrónico</label>
        <input type="text" class="form-control" name="email" id="inputEmail" maxlength="150" value="{{old('email',$user->email)}}" />
    </div>
    <div class="form-group col-12 col-md-6">
        <label for="inputRole"><span class="campo_obrigatorio text-danger small">&#10033;&nbsp;</span>Função</label>
        @if (Auth::user()->id == $user->id)
        <input type="hidden" class="form-control" name="role" id="inputRole" disabled value="{{old('role', $user->role)}}">
        <input type="text" class="form-control" disabled value="{{$user->roleToStr()}}">
        @else
        <select class="form-control" name="role" id="inputRole">
            <option {{old('role',$user->role)=='Admin'?"selected":""}} value="Admin">Administrador</option>
            <option {{old('role',$user->role)=='Employer'?"selected":""}} value="Employer">Funcionário</option>
        </select>
        @endif
    </div>
</div>
<br>
