@extends('layouts.app')

@section('title', 'Restaurar Password')

@section('content')
<div class="password-reset-form">
    <div>
        <div>
            <p class="title">Restaurar Password</p>
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
            @endif

            <form method="POST" action="{{ route('password.email') }}">
                @csrf
                <div>
                    <div>
                        <p class="labels">E-Mail</p>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <br>
                <div>
                    <div>
                        <button type="submit" class="btn btn-primary submit-button">
                            {{ __('Enviar Link de Recuperação') }}
                        </button>
                        <a class="btn btn-link action-buttons" href="{{ route('login') }}">
                            {{ __('Iniciar Sessão') }}
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @endsection
