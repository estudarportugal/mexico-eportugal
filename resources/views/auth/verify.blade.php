@extends('layouts.app')

@section('title', 'Ativação da Conta')

@section('content')
<div class="account-confirmation-form">
    <div>
        <div>
            <p class="title">Ativação de conta - <b style="color:black;">{{$user->name}}</b></p>
            <br>
            <p class="title">Escolha uma password</p>
            <form method="POST" action="{{route('confirmation.update', $user)}}">
                @csrf
                <div>
                    <div>
                        <p class="labels">Password</p>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" autofocus>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <br>
                <div>
                    <div>
                        <p class="labels">Confirmar Password</p>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <br>
                <div>
                    <div>
                        <button type="submit" class="btn submit-button">
                            Guardar Password
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
