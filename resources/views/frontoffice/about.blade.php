@extends('frontoffice.master-front')

@section('title', 'Acerca')

@section('content')

    @include('frontoffice.partials.header')

    <!-- CSS Link -->
    <link href="{{asset('css/style-about.css')}}" rel="stylesheet">

    <section>
        <div class="overlay hero-image"></div>
        <div class="overlay_content">
            <div data-sal="slide-up" data-sal-duration="1000" data-sal-delay="100"
                 data-sal-easing="ease-out-back">
                <h1>¿Quieres conocernos mejor?</h1>
                <p style="font-size:17px">¡Conoce un poco más sobre estudiar Portugal!<br> Te mostraremos todo y las
                    diferentes
                    experiencias
                    que poderás tener Estudar Portugal en México.</p>

                <div class="row text-center row_info mt-5 contagem">
                    <div class="col text-uppercase ">
                        <span style="font-weight: 700; font-size: 40px">+</span><h3 id="number" style="display:inline-block;font-weight: 700; font-size: 40px">300</h3><br><span style="font-size:18px">Estudiantes</span>
                    </div>
                    <div class="col text-uppercase">
                        <h3 id="number1" style="font-weight: 700; font-size: 40px">7</h3><span style="font-size:18px">Universidades</span>
                    </div>
                    <div class="col text-uppercase">
                        <h3 id="number2" style="font-weight: 700; font-size: 40px">6</h3><span style="font-size:18px">Países</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="container textos_comuns_Intro">
        <h3 class="font-weight-bold textos_comuns" style="text-align: center" data-sal="slide-up"
            data-sal-duration="1000" data-sal-delay="100" data-sal-easing="ease-out-back">Estudar Portugal en
            México</h3>
        <p id="textIntro" style="line-height:1.6" data-sal="slide-up" data-sal-duration="1000"
           data-sal-delay="200" data-sal-easing="ease-out-back">Estudar Portugal es representado en México por
            ZOIS
            Consultores Académicos. <br> ZOIS es una agencia educativa mexicana que trabaja de cerca con redes
            de
            colegios y universidades con el propósito de asesorar a los jóvenes en la selección de su carrera y
            en todos los tramites para irse a Estudiar en Portugal en Unión Europea.</p>

        <br><br><br><br>
    </div>

    <div class="bg-light">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="/media/about.png" class="img-fluid imgem_rapariga" alt="" style="width: 70%;">
                </div>

                <div class="col-md-6 mb-5 mt-5 textos_missao_visao" data-sal="slide-left"
                     data-sal-duration="1000" data-sal-delay="100" data-sal-easing="ease-out-back">
                    <br><br>
                    <h3 class="textos_comuns">Misión</h3>
                    <p style="line-height:1.5">Apoyar y ayudar a todos los que quieran
                        cumplir sus sueños en el mundo afuera de su país.</p>
                    <br>
                    <h3 class="textos_comuns mt-5">Visión</h3>
                    <p style="line-height:1.5">Desarrollo personal académico y
                        profesional de los jóvenes através de su capacitación y formación académica.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="bg-light">
        <div class="container">
            <div class="row" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="100"
                 data-sal-easing="ease-out-back">
                <div class="col-md-12 mb-5 mt-4 textos_missao_visao_respons">
                    <br><br>
                    <h3 class="textos_comuns text-center">Misión</h3>
                    <p style="line-height:1.5; text-align: center">Apoyar y ayudar a todos los que quieran
                        cumplir sus sueños en el mundo afuera de su país.</p>
                    <br>
                    <h3 class="textos_comuns mt-5 text-center">Visión</h3>
                    <p style="line-height:1.5; text-align: center">Desarrollo personal académico y
                        profesional de los jóvenes através de su capacitación y formación académica.</p>
                </div>
            </div>
        </div>
    </div>

    <br><br><br>

    <div class="container">

        <h3 class="font-weight-bold mb-4 textos_comuns" style="text-align: center" data-sal="slide-up"
            data-sal-duration="1000" data-sal-delay="100" data-sal-easing="ease-out-back">¿Porque estudiar en
            Portugal?</h3>
        <p id="textIntro1" style="line-height:1.6" data-sal="slide-up"
           data-sal-duration="1000" data-sal-delay="200" data-sal-easing="ease-out-back">A Estudar Portugal es una
            organización que nasció y se hay
            desarrollado adentro de
            las universidades portuguesas. Es una consultora educativa que apoya y ayuda a todos
            los que quieran superar sus proprias limitaciones, explorando un mundo de
            oportunidades acádemicas que el futuro y Portugal ofrece.</p>

        <br><br><br>

        <div class="row" id="cardValores">

            <div class="col-md-4">
                <div class="card" style="height:80%!important" data-sal="slide-up"
                     data-sal-duration="1000" data-sal-delay="100" data-sal-easing="ease-out-back">
                    <div class="card-body">
                        <i class="far fa-handshake mb-3" style="font-size: 50px; color: rgb(0, 64, 118);"></i>
                        <h5 class="text-uppercase font-weight-bold text-black-60">Excelencia</h5>
                        <p>Todas las Universidades son rankeadas por su calidad. Los titulos son reconocidos
                            mundialmente</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card" style="height:80%!important" data-sal="slide-up"
                     data-sal-duration="1000" data-sal-delay="200" data-sal-easing="ease-out-back">
                    <div class="card-body">
                        <i class="fas fa-shield-alt mb-3" style="font-size: 50px; color: rgb(0, 64, 118);"></i>
                        <h5 class="text-uppercase font-weight-bold text-black-60">Seguridad</h5>
                        <p>VIVIR y ESTUDIAR en Portugal es muy seguro y tranquilo. Al final del dia, a cualquier
                            hora podrá
                            salir a caminar por la ciudad.
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card" style="height:80%!important" data-sal="slide-up"
                     data-sal-duration="1000" data-sal-delay="300" data-sal-easing="ease-out-back">
                    <div class="card-body">
                        <i class="fas fa-globe mb-3" style="font-size: 50px; color: rgb(0, 64, 118);"></i>
                        <h5 class="text-uppercase font-weight-bold text-black-60">Europa</h5>
                        <p>Todo el estudiante en Portugal, será un estudiante EUROPEO. Podrás participar en
                            ERASMUS
                            en
                            cualquier pais europeu.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card" style="height:80%!important" data-sal="slide-up"
                     data-sal-duration="1000" data-sal-delay="400" data-sal-easing="ease-out-back">
                    <div class="card-body">
                        <i class="fas fa-euro-sign mb-3" style="font-size: 50px; color: rgb(0, 64, 118);"></i>
                        <h5 class="text-uppercase font-weight-bold text-black-60">Economico</h5>
                        <p>Vivir y estudiar en Portugal es muy barato. El alojamiento, la alimentación son
                            bastante
                            acessibles y baratos.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card" style="height:80%!important" data-sal="slide-up"
                     data-sal-duration="1000" data-sal-delay="500" data-sal-easing="ease-out-back">
                    <div class="card-body">
                        <i class="fas fa-graduation-cap mb-3"
                           style="font-size: 50px; color: rgb(0, 64, 118);"></i>
                        <h5 class="text-uppercase font-weight-bold text-black-60">Futuro</h5>
                        <p>Todos los programas son validados y reconocidos mundialmente. Será siempre un
                            PASSAPORTE
                            para tu
                            futuro.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card" id="valores" style="height:80%!important; background: rgba(0,84,144,0.8)"
                     data-sal="slide-up"
                     data-sal-duration="1000" data-sal-delay="600" data-sal-easing="ease-out-back">
                    <div class="card-body">
                        <h3 class="text-uppercase font-weight-bold" style="padding-top: 25%; padding-bottom: 25%;color: white">
                            Valores</h3>
                    </div>
                </div>
            </div>

        </div>
    </div>




    <!--Ficheiro de JS-->
    <script type="text/javascript" src="/js/script-about.js"></script>
@endsection
