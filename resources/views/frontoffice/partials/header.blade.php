<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title> @yield('title') | Mexico Estudar Portugal</title>

    <meta name="keywords" content="Estudar, Portugal, Universidade, Estudar Portugal, México, @yield('tags')">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="/media/favicon.png">

    <!-- Sal JS -->
    <link rel="stylesheet" href="{{asset('vendor/sal/dist/sal.css')}}">

    <!-- CSS Link -->
    <link rel="stylesheet" href={{asset('css/style-master.css')}}>
    <!-- Bootstrap core CSS -->
    <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Fontawesome core CSS -->
    <link href="{{asset('vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:300,500&display=swap" rel="stylesheet">
</head>