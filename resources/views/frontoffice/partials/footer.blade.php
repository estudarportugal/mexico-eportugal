<link href="{{asset('css/style-master.css')}}" rel="stylesheet">

<footer class="footer page-footer font-small mdb-color pt-4 py-2 mt-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3 col-lg-2" id="logofooter">
                <a href="{{route('index')}}">
                  <img src="{{asset('media/logo-zois-footer.png')}}" alt="Logótipo - Estudar Portugal" style="width: 300px">
                </a>
            </div>
            <div class="col-12 col-md-9 col-lg-10 mb-5 miniMenu">
                <ul class="list-inline small footer">
                    <li class="list-inline-item"><a href="{{route('index')}}">Inicio</a></li>
                    <li class="list-inline-item"><a href="{{route('about')}}">Acerca</a></li>
                    <li class="list-inline-item"><a href="{{route('contacts')}}">Contacto</a></li>
                    <li class="list-inline-item"><a href="{{route('blog')}}">Blog</a></li>
                </ul>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-7 col-lg-8 small text-muted" style="color: #0f67a0 !important;">
                <p class="text-center text-md-left">&copy;
                    <script>
                        document.write(new Date().getFullYear());
                    </script>
                    Derechos de autor. Estudiar Portugal Todos los derechos reservados.
                </p>
            </div>
            <div class="col-md-5 col-lg-4 ml-lg-0">
                <div class="text-center text-md-right">
                    <ul class="list-unstyled list-inline" style="margin-top: -5px; margin-right:-15px">
                        <li class="list-inline-item">
                            <a href="https://www.instagram.com/estudarportugalpt/" class="btn-floating" target="_blank">
                                <i class="fab fa-instagram" style="font-size: 20px; color: rgb(0, 64, 118)" title="Estudiar Portugal en Instagram"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://www.facebook.com/EstudarPortugalPT" class="btn-floating btn-md" target="_blank">
                                <i class="fab fa-facebook-f" style="font-size: 20px; color: rgb(0, 64, 118)" title="Estudiar Portugal en Facebook "></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://www.youtube.com/" class="btn-floating" target="_blank">
                                <i class="fab fa-youtube" style="font-size: 20px; color: rgb(0, 64, 118)" title="Estudiar Portugal en Youtube"></i>
                            </a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>

    <!-- GetButton.io widget -->
    <script type="text/javascript">
        (function () {
            var options = {
                facebook: "112679410281101", // Facebook page ID
                whatsapp: "+351917804598", // WhatsApp number
                call_to_action: "Hola ¿En qué podemos ayudarlo?", // Call to action
                button_color: "#f1c930", // Color of button
                position: "right", // Position may be 'right' or 'left'
                order: "facebook,whatsapp", // Order of buttons
            };
            var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
            s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
            var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
        })();
    </script>
    <!-- /GetButton.io widget -->

</footer>

<script>
    sal();
</script>
