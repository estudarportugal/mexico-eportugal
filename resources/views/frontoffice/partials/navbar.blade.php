<!-- CSS Link -->
<link rel="stylesheet" href="{{asset('css/style-master.css')}}">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,500,500i,600,600i,700,700i,800&display=swap"
      rel="stylesheet">

<nav class="navbar navbar-expand-lg bg-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="{{route('index')}}">
            <img id="logo-EP" src="{{asset('media/logo.png')}}" alt="Logótipo - Estudar Portugal">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <div class="menu-button"></div>
            <div class="menu-button"></div>
            <div class="menu-button"></div>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link line_nav" href="{{route('index')}}">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link line_nav" href="{{route('about')}}">Acerca</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link line_nav" href="{{route('blog')}}">Blog</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link line_nav" href="{{route('contacts')}}">Contacto</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
