<link href="{{asset('css/style-landing-page.css')}}" rel="stylesheet">

<footer class="page-footer font-small">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <a href="{{route('index')}}" target="_blank" id="logoFooter">
                    <img src="{{asset('media/logo-branco.png')}}" alt="Logótipo - Estudar Portugal">
                </a>
                <p id="textFooter">Estudar Portugal es un equipo de profesionales <br> profundamente comprometidos con
                    el
                    desarrollo <br> integral de dos estudios a escala global.</p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <p style="font-weight: bold">Mexico</p>
                <p>{{$landingpage->contact1}} <br> {{$landingpage->contact2}} </p>
                <p>
                    Sitio oficial de Mexico - Estudar Portugal
                    <a href="https://mexico.estudarportugal.com/" target="_blank"
                       title="Website do Mexico - Estudar Portugal"> www.mexico.estudarportugal.com </a>
                </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12">
                <p style="font-weight: bold">Estudar Portugal</p>
                <a href="https://www.facebook.com/EstudarPortugalPT" class="icon-bg" target="_blank"
                   title="Facebook Estudar Portugal">
                    <i class="fab fa-facebook-f"></i>
                </a>
                <a href="https://www.instagram.com/estudarportugalpt/" class="icon-bg" target="_blank"
                   title="Instagram Estudar Portugal">
                    <i class="fab fa-instagram"></i>
                </a>
                <a href="#" class="icon-bg" target="_blank" title="Whatsapp Estudar Portugal">
                    <i class="fab fa-whatsapp"></i>
                </a>
                <p>
                    Sitio oficial de Estudar Portugal
                    <a href="https://estudarportugal.com/" target="_blank" title="Website Estudar Portugal">
                        www.estudarportugal.com </a>
                </p>
            </div>
        </div>

        <hr>

        <div class="row footerBg">
            <div class="col-lg-12 small">
                <p class="text-center">&copy;
                    <script>
                        document.write(new Date().getFullYear());
                    </script>
                    Estudar Portugal. Todos los derechos reservados | Política de privacidad
                </p>
            </div>
        </div>
    </div>


    <!-- GetButton.io widget -->
    <script type="text/javascript">
        (function () {
            var options = {
                facebook: "112679410281101", // Facebook page ID
                whatsapp: "+351917804598", // WhatsApp number
                call_to_action: "Hola ¿En qué podemos ayudarlo?", // Call to action
                button_color: "#f1c930", // Color of button
                position: "right", // Position may be 'right' or 'left'
                order: "facebook,whatsapp", // Order of buttons
            };
            var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
            s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
            var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
        })();
    </script>
    <!-- /GetButton.io widget -->
    <!-- /GetButton.io widget -->

</footer>

<script src="/vendor/bootstrap/js/jquery-3.4.1.min.js"></script>
<script src="/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="/vendor/sal/dist/sal.js"></script>

<script>
    sal();
</script>
