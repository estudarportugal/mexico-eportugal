<!-- Modal Nautica -->
<div class="modal fade" id="nautica" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="color: white; background-color: #005c9c">
                <h5 class="modal-title" id="exampleModalLongTitle">Escola Náutica Infante D. Henrique</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: white">&times;</span>
                </button>
            </div>
            <div class="modal-header text-uppercase"><b>Licenciaturas</b></div>
            <div class="modal-body">
                - Gestión de Transportes y Logística<br>
                - Gestión Portuária<br>
                - Ingeniería de Máquinas Marítimas<br>
                - Ingeniería Eletrotécnica Marítima<br>
                - Pilotaje<br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal UALG -->
<div class="modal fade" id="ualg" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="color: white; background-color: #005c9c">
                <h5 class="modal-title" id="exampleModalLongTitle">Universidade do Algarve</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: white">&times;</span>
                </button>
            </div>
            <div class="modal-header text-uppercase"><b>Licenciaturas</b></div>
            <div class="modal-body">
                - Agronomía<br>
                - Arquitectura Paisajista<br>
                - Artes Visuales<br>
                - Bioingeniería<br>
                - Biología<br>
                - Biología Marina<br>
                - Bioquímica<br>
                - Biotecnología<br>
                - Ciencias Biomédicas<br>
                - Ciencias de la Comunicación<br>
                - Ciencias de la Educación y de la Formación<br>
                - Ciencias Farmacéuticas<br>
                - Deporte<br>
                - Design de la Comunicación<br>
                - Economía<br>
                - Educación Social<br>
                - Enfermería<br>
                - Farmacia<br>
                - Gestión<br>
                - Gestión de Empresas<br>
                - Gestión Hotelera<br>
                - Gestión Marina y Costera<br>
                - Idiomas y Comunicación<br>
                - Idiomas, Literaturas y Culturas<br>
                - Imagen Animada<br>
                - Imagen Médica y Radioterapia<br>
                - Ingeniería Alimentaria<br>
                - Ingeniería Civil<br>
                - Ingeniería Eléctrica y Electrónica<br>
                - Ingeniería Informática<br>
                - Ingeniería Mecánica<br>
                - Marketing<br>
                - Matemática Aplicada a la Economía y Gestión<br>
                - Nutrición y Dietética<br>
                - Patrimonio Cultural y Arqueología<br>
                - Psicología<br>
                - Sociología<br>
                - Turismo<br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal UBI -->
<div class="modal fade" id="ubi" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="color: white; background-color: #005c9c">
                <h5 class="modal-title" id="exampleModalLongTitle" >Universidade Beira Interior</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: white">&times;</span>
                </button>
            </div>
            <div class="modal-header text-uppercase"><b>Licenciaturas</b></div>
            <div class="modal-body">
                - Arquitectura<br>
                - Bioingeniería<br>
                - Bioquímica<br>
                - Biotecnología<br>
                - Ciencia Política y Relaciones Internacionales<br>
                - Ciencias Biomédicas<br>
                - Ciencias de la Comunicación<br>
                - Ciencias de la Cultura<br>
                - Ciencias del Deporte<br>
                - Ciencias Farmacéuticas<br>
                - Cinema<br>
                - Design de Moda<br>
                - Design Industrial<br>
                - Design Multimedia<br>
                - Economía<br>
                - Estudios Portugueses y Españoles<br>
                - Gestión<br>
                - Informática Web<br>
                - Ingeniería Aeronáutica<br>
                - Ingeniería Civil<br>
                - Ingeniería Electromecánica<br>
                - Ingeniería Electrotécnica e de Computadores<br>
                - Ingeniería Informática<br>
                - Ingeniería y Gestión Industrial<br>
                - Marketing<br>
                - Matemática y Aplicaciones<br>
                - Medicina<br>
                - Optometría y Ciencias da Visón<br>
                - Psicología<br>
                - Química Industrial<br>
                - Sociología<br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal ISCTE-->
<div class="modal fade" id="iscte" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="color: white; background-color: #005c9c">
                <h5 class="modal-title" id="exampleModalLongTitle">Iscte - Instituto Universitário de Lisboa</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: white">&times;</span>
                </button>
            </div>
            <div class="modal-header text-uppercase"><b>Licenciaturas</b></div>
            <div class="modal-body">
                - Antropología<br>
                - Arquitectura<br>
                - Ciencia de los Datos<br>
                - Ciencia Política<br>
                - Economía<br>
                - Finanzas y Contabilidad<br>
                - Gestión<br>
                - Gestión de Marketing<br>
                - Gestión de Recursos Humanos<br>
                - Gestión Industrial y Logística<br>
                - Historia Moderna e Contemporánea<br>
                - Informática y Gestión de Empresas<br>
                - Ingeniería de Telecomunicaciones y Informática<br>
                - Ingeniería Informática<br>
                - Psicología<br>
                - Servicio Social<br>
                - Sociologia<br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
