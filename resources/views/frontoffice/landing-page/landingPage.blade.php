@extends('frontoffice.landing-page.master-landing')

@section('title', $landingpage->title)

@section('tags', $landingpage->tags)

@section('content')
    <!-- CSS Link -->
    <link href="{{asset('css/style-landing-page.css')}}" rel="stylesheet">

    <div id="imageBg_1"
         style="background-image: linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.5)), url('{{ asset('/storage/landingpages-images/'.$landingpage->backgroundimage1) }}')">

        <div class="container">

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 mt-4">
                    <a href="{{route('index')}}" target="_blank" id="logoFooter">
                        <img src="{{asset('media/logo-branco.png')}}" alt="Logótipo - Estudar Portugal">
                    </a>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 mt-4 icones">
                    <a href="https://www.facebook.com/EstudarPortugalPT" class="icon-bg-header" target="_blank"
                       title="Facebook Estudar Portugal">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <a href="https://www.instagram.com/estudarportugalpt/" class="icon-bg-header" target="_blank"
                       title="Instagram Estudar Portugal">
                        <i class="fab fa-instagram"></i>
                    </a>
                    <a href="#" class="icon-bg-header" target="_blank" title="Whatsapp Estudar Portugal">
                        <i class="fab fa-whatsapp"></i>
                    </a>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 overlay_content" data-sal="slide-right"
                     data-sal-duration="1000" data-sal-delay="500" data-sal-easing="ease-out-back">
                    <h1>{{$landingpage->title}}</h1>
                    <br>
                    <p>{{$landingpage->description}}</p>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 overlay_content overlay_content_padding">
                    <h2 class="text-center">¡QUIERO SABER MÁS!</h2>
                    <form>
                        <div class="form-group shadow-sm">
                            <input type="text" class="form-control" id="exampleInputNome" aria-describedby="nomeHelp"
                                   placeholder="Escribe tu nombre" required style="height: 50px">
                        </div>
                        <div class="form-group shadow-sm">
                            <input type="email" class="form-control" id="exampleInputEmail"
                                   placeholder="Escribe tu email" required style="height: 50px">
                        </div>
                        <div class="form-group shadow-sm">
                            <input type="email" class="form-control" id="exampleInputEmail"
                                   placeholder="Escribe tu número de Whatsapp" required style="height: 50px">
                        </div>

                        <div class="form-group shadow-sm">
                            <select class="form-control" id="exampleFormControlSelect1" required style="height: 50px">
                                <option hidden>Nivel estudios actuales</option>
                                <option>Bachillerato completo</option>
                                <option>Bachillerato en curso</option>
                                <option>Ya estoy en una universidad</option>
                                <option>Otro</option>
                            </select>
                        </div>
                        <button type="submit" class="btn shadow-sm">
                            Enviar
                        </button>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <div class="container textOverlayContentBlue">
        <h4 data-sal="slide-up" data-sal-duration="1000" data-sal-delay="200" data-sal-easing="ease-out-back">EUROPA EN
            TU FUTURO</h4>
        <div class="row mt-3 mb-3">
            <div class="col-md-4" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="300"
                 data-sal-easing="ease-out-back">
                <i class="fas fa-award mb-3"></i>
                <p style="font-size: 17px">Universidad en la mejor lista del mundo</p>
            </div>
            <div class="col-md-4" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="400"
                 data-sal-easing="ease-out-back">
                <i class="fas fa-coins mb-3"></i>
                <p style="font-size: 17px">Costos muy accesibles</p>
            </div>
            <div class="col-md-4" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="500"
                 data-sal-easing="ease-out-back">
                <i class="fas fa-shield-alt mb-3"></i>
                <p style="font-size: 17px">Tercer país más seguro del mundo</p>
            </div>
        </div>
    </div>

    <div id="imageBg_2"
         style="background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('{{ asset('/storage/landingpages-images/'.$landingpage->backgroundimage2) }}')">

        <div class="container textOverlayContent">

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12" data-sal="slide-up" data-sal-duration="1000"
                     data-sal-delay="500" data-sal-easing="ease-out-back">
                    <h4>Ven a conocernos.</h4>
                    <h5>¡Te estamos esperando!</h5>
                    <button type="button" class="btn mt-2" title="Saber más">
                        <a target="_blank" href="http://mexico.eportugal.test/"
                           style="text-decoration: none; color: white">
                            Saber más
                        </a>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div id="imageBg_3">
        <div class="container colaboradoresTextWhite">
            <h4>Testimonios de Estudar Portugal</h4>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            {{-- Indicadores de slide --}}
                            {{--Daniel Meti o '=' para aparecer a quantidade de circulos
                             (total em vez de só três, porque depois aparecia só tres circulos para 4 testemunhos)--}}
                            @if(count($testimonials))
                                @foreach ($testimonials as $k => $testimonial)
                                    @if ($k >= 0)
                                        <li data-target="#carouselExampleIndicators" data-slide-to="{{$k}}"></li>
                                    @endif
                                @endforeach
                            @else
                                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            @endif
                        </ol>

                        {{-- Items do carousel --}}

                        @if(count($testimonials))
                            <div class="carousel-inner text-center">
                                <div class="carousel-item active">
                                    <div class="foto_testemunha">
                                        <img
                                            src="{{Storage::disk('public')->url('testimonials-photos/').$testimonials[0]->photo}}">
                                    </div>
                                    <div class="testemunhos">
                                        <p class="font-weight-bold">{{$testimonials[0]->name}}</p>
                                        <div class="text-center"
                                             style="margin: 0 50px">{{$testimonials[0]->description}}
                                        </div>
                                    </div>
                                </div>
                                @foreach ($testimonials as $k => $testimonial)
                                    @if ($k > 0)
                                        <div class="carousel-item">
                                            <div class="foto_testemunha">
                                                <img
                                                    src="{{Storage::disk('public')->url('testimonials-photos/').$testimonial->photo}}">
                                            </div>
                                            <div class="testemunhos">
                                                <p class="font-weight-bold">{{$testimonial->name}}</p>
                                                <div class="text-center"
                                                     style="margin: 0 50px">{{$testimonial->description}}
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                                @else
                                    <div class="carousel-inner text-center">
                                        <div class="carousel-item active">
                                            <div class="foto_testemunha">
                                                <img src="{{asset('media/default_user.png')}}">
                                            </div>
                                            <div class="testemunhos">
                                                <p class="font-weight-bold">Sem nome</p>
                                                <div class="text-center" style="margin: 0 50px">&nbsp;</div>
                                            </div>
                                        </div>
                                        @endif

                                    </div>

                                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                                       data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                                       data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                            </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container text-center mb-5">
            <div class="row mt-5 text-center" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="200" data-sal-easing="ease-out-back">
                <div class="col"><p style="font-size:24px;font-weight: 700;">Alianzas</p></div>
                
            </div>
    
            <div class="row text-center">
    
                <div class="col col-lg-3 col-sm-6 col-12 p-2" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="300" data-sal-easing="ease-out-back">
                    <img src="{{asset('media/iscte.png')}}" title="Instituto Universitário de Lisboa" alt="Logótipo - Instituto Universitário de Lisboa" style="width:100%;max-width:200px">
                </div>
    
                <div class="col col-lg-3 col-sm-6 col-12 p-2" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="400" data-sal-easing="ease-out-back">
                    <img src="{{asset('media/Ualg.png')}}" title="Universidade do Algarve" alt="Logótipo - Universidade do Algarve" style="width:100%;max-width:200px">
                </div>
    
                <div class="col col-lg-3 col-sm-6 col-12 p-2" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="500" data-sal-easing="ease-out-back">
                    <img src="{{asset('media/UBI.png')}}" title="Universidade Beira Interior" alt="Logótipo - Universidade Beira Interior" style="width:100%;max-width:200px">
                </div>
    
                <div class="col col-lg-3 col-sm-6 col-12 p-2" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="600" data-sal-easing="ease-out-back">
                    <img src="{{asset('media/Nautica.png')}}" title="Escola Superior Náutica" alt="Logótipo - Escola Superior Náutica" style="width:100%;max-width:200px">
                    
                </div>
    
            </div>
          
        </div>



    </div>
@endsection
