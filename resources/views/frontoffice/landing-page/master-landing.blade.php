<!DOCTYPE html>
<html lang="pt" dir="ltr">

@include('frontoffice.landing-page.partials.header')

<body>
<script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>

<script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<script src="{{asset('vendor/sal/dist/sal.js')}}"></script>


@yield('content')

@include('frontoffice.landing-page.partials.footer')

</body>

</html>
