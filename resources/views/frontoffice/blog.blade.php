@extends('frontoffice.master-front')

@section('title', 'Blog')

@section('content')

    <!-- CSS Link -->
    <link href="{{asset('css/style-blog.css')}}" rel="stylesheet">

    @include('frontoffice.partials.header')

    <div class="hero-image">
        <div class="container">
            <div class="hero-text" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="100"
                 data-sal-easing="ease-out-back">
                <h1>¿Quieres saber qué hay de nuevo?</h1>
                <p>Clique no botão abaixo para descobrir as mais recentes<br>notícias relacionadas com a atividade do
                    Estudar Portugal!</p>
                <br>
                <a type="button" href="#container">Saber mas</a>
            </div>
        </div>
    </div>
    <div id="container">
        <div id="main">
            <div class="container justify-content-center">
                <h3>Ultimas Noticias</h3>
                <div class="row">
                    @if (count($posts))
                        @foreach($posts as $post)
                            @if (empty($post->deleted_at))
                                <div class="col-lg-4 col-md-6">
                                    <div class="card"  title="Ver Más {{$post->title}}">
                                        <a class="post-link" href="{{route('blogDetail', $post->id)}}">
                                            <div class="inner">
                                                <div class="card-img-to card_img animatic_img" style="background-image: url('{{ asset('/storage/posts-images/'.$post->image) }}')"></div>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-title">{{$post->title}}</h5>
                                                <p class="card-text text-truncate">{{$post->resume}}</p>
                                            </div>
                                            <div class="card-footer">
                                                <small
                                                    class="text-muted">Publicado: {{date('d/m/Y', strtotime($post->created_at))}}</small>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @else
                        <h6 class="msgDivulgacoes">Não Existe Publicações Disponíveis!</h6>
                    @endif
                </div>
            </div>
            <br>
            <div class="col-lg-12">
                <ul class="pagination justify-content-center text-black">
                    {{ $posts->links() }}
                </ul>
            </div>
        </div>
    </div>
@endsection
