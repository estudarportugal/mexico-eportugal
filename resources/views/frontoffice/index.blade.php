@extends('frontoffice.master-front')

@section('title', 'Inicio')

@section('content')

    <!-- CSS Link -->


    @include('frontoffice.partials.header')


    <link href="{{asset('css/style-index.css')}}" rel="stylesheet">

    @include('frontoffice.partials.modal')

    <section>

        <div class="overlay"></div>
        <video src="{{asset('media/video/video.mp4')}}" poster="{{asset('media/poster.jpg')}}" autoplay loop
               muted></video>
        <div class="overlay_content">

            <h3>Bienvenido a</h3>
            <h1>ESTUDAR PORTUGAL</h1>

            <div class="row text-center row_info mt-5">
                <div class="col border-right text-uppercase anosactiv">
                    <div id="number" class="row_info_cursos">114</div>
                    <span
                        class="row_info_cursos_label">Licenciaturas</span>
                </div>
                <div class="col middle-col-info border-right text-uppercase">
                    <div id="number1" class="row_info_cursos">170</div>
                    <span class="row_info_cursos_label">Maestrías</span>
                </div>
                <div class="col text-uppercase">
                    <div id="number2" class="row_info_cursos">71</div>
                    <span class="row_info_cursos_label">Doctorados</span>
                </div>
            </div>
        </div>
    </section>

    <div class="container index">
        <!-- 1 section -->
        <div class="main">
            <!-- 2 section -->
            <br>

            <h3 class="font-weight-bold textos_comuns mt-3 " style="text-align: center;" data-sal="slide-up"
                data-sal-delay="500" data-sal-easing="ease-out-back">¿Por qué Estudiar en
                Portugal?</h3><br>

            {{-- FULL SCREEN --}}


            <div class="row rounded shadow pl-2 pr-2 pt-4 pb-4 text-center mt-5 cards_Portugal" data-sal="slide-up"
                 data-sal-delay="300" data-sal-easing="ease-out-back">

                <div class="col-sm mt-2 border-right">
                    <div class="rounded-circle circuloIcone"></div>
                    <i class="far fa-handshake mb-3 icone text-warning" style="font-size: 50px!important;"></i>
                    <br><br><br>
                    <h5 class="text-uppercase font-weight-bold text-black-60 mb-3">Excelencia</h5>
                    <p> Todas las Universidades son rankeadas por su calidad. Los títulos son reconocidos mundialmente
                    </p>
                </div>
                <div class="col-sm mt-2 border-right">
                    <div class="rounded-circle circuloIcone"></div>
                    <i class="fas fa-shield-alt mb-3 text-success icone" style="font-size: 50px!important;"></i>
                    <br><br><br>
                    <h5 class="text-uppercase font-weight-bold text-black-60 mb-3">Seguridad</h5>
                    <p> VIVIR y ESTUDIAR en Portugal es muy seguro y tranquilo. Al final del día, a cualquier hora podrá
                        salir a
                        caminar por la ciudad.</p>
                </div>
                <div class="col-sm mt-2">
                    <div class="rounded-circle circuloIcone"></div>
                    <i class="fas fa-graduation-cap mb-3 icone text-info"
                       style="font-size: 50px!important;"></i>
                    <br><br><br>
                    <h5 class="text-uppercase font-weight-bold text-black-60 mb-3">Futuro</h5>
                    <p> Todos los programas son validados y reconocidos mundialmente. Será siempre un PASAPORTE para tu
                        futuro.</p>
                </div>
            </div>


            {{-- Mobile --}}
            <div class="text-center cardsRespons">


                <div class="row m-4" data-sal="slide-up" data-sal-delay="500" data-sal-easing="ease-out-back">
                    <div class="col card circle shadow">
                        <div class="innerbtnx">
                            <i class="far fa-handshake text-warning mb-3"
                               style="font-size: 35px!important; color: rgb(0, 64, 118); margin-top: 30px!important;"></i>
                            <h5 class="text-uppercase font-weight-bold text-black-60 mb-3">Excelencia</h5>
                            <p style="margin-right: 5px; margin-left: 5px"> Todas las Universidades son rankeadas por su
                                calidad. Los títulos son reconocidos
                                mundialmente</p>
                        </div>
                    </div>
                </div>

                <div class="row m-4" data-sal="slide-up" data-sal-delay="300" data-sal-easing="ease-out-back">
                    <div class="col card rounded shadow">
                        <i class="fas fa-shield-alt text-success mb-3"
                           style="font-size: 35px!important; color: rgb(0, 64, 118); margin-top: 30px!important;"></i>
                        <h5 class="text-uppercase font-weight-bold text-black-60 mb-3">Seguridad</h5>
                        <p style="margin-right: 5px; margin-left: 5px"> VIVIR y ESTUDIAR en Portugal es muy seguro y
                            tranquilo. <br> Al final del día, a cualquier hora podrá salir a caminar por la ciudad.</p>
                    </div>
                </div>

                <div class="row m-4" data-sal="slide-up" data-sal-delay="300" data-sal-easing="ease-out-back">
                    <div class="col card rounded shadow">
                        <i class="fas fa-graduation-cap text-info mb-3"
                           style="font-size: 35px!important; color: rgb(0, 64, 118); margin-top: 30px!important;"></i>
                        <h5 class="text-uppercase font-weight-bold text-black-60 mb-3">Futuro</h5>
                        <p style="margin-right: 5px; margin-left: 5px"> Todos los programas son validados y reconocidos
                            mundialmente. <br> Será siempre un PASAPORTE para tu futuro.</p>
                    </div>
                </div>
            </div>


        </div>

        <br><br>

        <!-- 3 section -->
        <h3 class="font-weight-bold mt-5 mb-4 textos_comuns" style="text-align: center" data-sal="slide-up"
            data-sal-delay="500" data-sal-easing="ease-out-back">
            ¿Es Visón General hasta ingresar a clases en Portugal?
        </h3>


        <div class="row btns-infos mt-5">


            {{-- Inscrição --}}
            <div class="col text-center steps_box" data-sal="slide-up" data-sal-delay="300"
                 data-sal-easing="ease-out-back">
                <button class="btn btnx" id="btn_inscripcion"></button>
                <p>
                <div style="font-weight: 700;">1er</div>
                <hr style="margin:0px 35%; border: 1px solid rgb(0, 64, 118)">
                <span style="font-size:18px">Inscripción</span>
                </p>
            </div>

            {{-- Postulacion --}}
            <div class="col text-center steps_box" data-sal="slide-up" data-sal-delay="300"
                 data-sal-easing="ease-out-back">
                <button class="btn btnx" id="btn_postulation"></button>
                <p>
                <div style="font-weight: 700;">2do</div>
                <hr style="margin:0px 35%; border: 1px solid rgb(0, 64, 118)">
                <span style="font-size:18px">Postulación</span>
                </p>
            </div>

            {{-- Matricula --}}
            <div class="col text-center steps_box" data-sal="slide-up" data-sal-delay="400"
                 data-sal-easing="ease-out-back">
                <button class="btn btnx" id="btn_matricula"></button>
                <p>
                <div style="font-weight: 700;">3er</div>
                <hr style="margin:0px 35%; border: 1px solid rgb(0, 64, 118)">
                <span style="font-size:18px">Monitoreo de cupos</span>
                </p>
            </div>

            {{-- Viaje --}}
            <div class="col text-center steps_box" data-sal="slide-up" data-sal-delay="500"
                 data-sal-easing="ease-out-back">
                <button class="btn btnx" id="btn_viaje"></button>
                <p>
                <div style="font-weight: 700;">4to</div>
                <hr style="margin:0px 35%; border: 1px solid rgb(0, 64, 118)">
                <span style="font-size:18px">Visa</span>
                </p>
            </div>

            {{-- Viaje Portugal --}}
            <div class="col text-center steps_box" data-sal="slide-up" data-sal-delay="600"
                 data-sal-easing="ease-out-back">
                <button class="btn btnx" id="btn_viajept"></button>
                <p>
                <div style="font-weight: 700;">5to</div>
                <hr style="margin:0px 35%; border: 1px solid rgb(0, 64, 118)">
                <span style="font-size:18px">Viaje y Acogida en Portugal</span>
                </p>
            </div>


        </div>


        <div class="container infos-mobile" data-sal="slide-up" data-sal-delay="500" data-sal-easing="ease-out-back">

            <div class="alert alert-info mb-2" role="button" data-toggle="collapse" data-target="#onediv">
                <div class="row">
                    <div class="col col-1 text-center">
                        <strong><i class="fa fas fa-pen-square"></i></strong>
                    </div>
                    <div class="col">
                        <span class="ml-2"><strong>1. Inscripción</strong></span>
                    </div>
                </div>

                <div id="onediv" class="collapse mt-2">
                    Para iniciar tu camino hacia Portugal, hacia Europa te deberás inscribir junto de nuestros
                    representantes. Para te inscribir, además de algunos documentos personales, te bastará tener un
                    documento de constancia que ateste tu nivel de estudios actuales, así como un documento de alguno
                    examen para ingreso al ensino superior que hayas rendido.
                </div>
            </div>


            <div class="alert alert-info mb-2" role="button" data-toggle="collapse" data-target="#twodiv">
                <div class="row">
                    <div class="col col-1 text-center">
                        <strong><i class="fa fas fa-file-contract"></i></strong>
                    </div>
                    <div class="col">
                        <span class="ml-2"><strong>2. Postulación</strong></span>
                    </div>
                </div>

                <div id="twodiv" class="collapse mt-2">
                    Para poder postular habrá que cumplir algunos requisitos esenciales. Para licenciatura, habrá que
                    haber concluido el bachillerato (preparatorio), así como de mínimo haber rendido el examen EXANI II
                    en la área o carrera a que apuntas. Para maestría vas a necesitar de tu título de licenciatura, y
                    para PhD será necesario contar con el título de tu maestría y tu curricular.
                </div>
            </div>


            <div class="alert alert-info mb-2" role="button" data-toggle="collapse" data-target="#threediv">
                <div class="row">
                    <div class="col col-1 text-center">
                        <strong><i class="fa far fa-address-card"></i></strong></span>
                    </div>
                    <div class="col">
                    <span class="ml-2"><strong>3. Monitoreo de cupos</strong>
                    </div>
                </div>

                <div id="threediv" class="collapse mt-2">
                    ¡El ingreso a las universidades se hace siempre por mérito! Para eso el equipo de asesores de
                    Estudar Portugal estará continuamente monitoreando las mejores posibilidades que tu tendrás. Es una
                    gran ventaja poder saber adónde se podrá matricular con suceso.
                </div>
            </div>


            <div class="alert alert-info mb-2" role="button" data-toggle="collapse" data-target="#fourdiv">
                <div class="row">
                    <div class="col col-1 text-center">
                        <strong><i class="fa fas fa-suitcase-rolling"></i></strong>
                    </div>
                    <div class="col">
                        <span class="ml-2"><strong>4. Visa</strong></span>
                    </div>
                </div>

                <div id="fourdiv" class="collapse mt-2">
                    Para venir a estudiar para Portugal es necesario una VISA estudiantil. Estudar Portugal como
                    organización de asesoría académica mantiene una relación muy cerca con universidades y embajadas
                    portuguesas para te poder apoyar en todos los momentos. Sin embargo, la VISA es y será siempre un
                    proceso personal e intransmisible!
                </div>
            </div>


            <div class="alert alert-info mb-2" role="button" data-toggle="collapse" data-target="#fivediv">
                <div class="row">
                    <div class="col col-1 text-center">
                        <strong><i class="fa fas fa-plane-departure"></i></strong>
                    </div>
                    <div class="col">
                        <span class="ml-2"><strong>5. Viaje y Acogida en Portugal</strong></span>
                    </div>
                </div>

                <div id="fivediv" class="collapse mt-2">
                    Estudar Portugal te estará esperando en aeropuerto para te recibir y trasladar hacia tu nueva
                    habitación en tu nueva casa :) Te garantizamos la seguridad, la tranquilidad y el conforto para que
                    tu nueva vida sea lo mejor posible.
                </div>
            </div>


        </div>


        <!-- 4 section -->
        <br>
        <h3 class="font-weight-bold textos_comuns mt-5 mb-3" style="text-align: center;" data-sal="slide-up"
            data-sal-delay="500" data-sal-easing="ease-out-back">¡Lo que hacemos por ti!</h3>


    </div>

    <div class="container-fluid text-white pt-5 pb-5" style="background-color:#4D799F" data-sal="slide-up" data-sal-delay="300"
    data-sal-easing="ease-out-back">

        <div class="container">

            <div class="row pt-3">
                <div class="col col-md-6 text-center pr-3 pt-3 infos" data-sal="slide-up" data-sal-delay="400"
                     data-sal-easing="ease-out-back">
                    <p><i class="fas fa-map-marked-alt infos_icon"></i><br>
                    <h2>Tu Futuro</h2>
                    ¡Estudiar en Portugal adentro de Unión Europea es increíble! Irás a vivir en un territorio seguro y
                    sin fronteras. Irás a conocer personas de todo el mundo, sin cualquier limitación. Irás a estudiar
                    en un paradigma de excelencia europeo. Estarás adentro de una cultura milienar con un sentido de
                    excelencia y oportunidad hacia el futuro…. ¡Lo tuyo!
                    </p>
                </div>
                <div class="col col-md-6 text-center pr-3 pt-3 infos" data-sal="slide-up" data-sal-delay="500"
                     data-sal-easing="ease-out-back">
                    <p><i class="fas fa-user-tie infos_icon"></i><br>
                    <h2>Equipo especializado</h2>
                    Estudar Portugal cuenta con un equipo de consultores y assessores
                    académicos especializados capaces para guiarte de acuerdo a tu vocación, objetivos profesionales y
                    académicos.
                    </p>
                </div>
            </div>

            <div class="row pt-3">
                <div class="col col-md-6 text-center pr-3 pt-4 infos" data-sal="slide-up" data-sal-delay="600"
                     data-sal-easing="ease-out-back">
                    <p><i class="fas fa-users infos_icon"></i><br>
                    <h2>Integración</h2>
                    Nos preocupamos con los jóvenes que eligan estudiar en Portugal – Europa. Somos la única agencia que
                    se preocupa por la integración y el bienestar de los estudiantes que eligen estudiar en Portugal.
                    Los esperamos en aeropuerto, los llevamos hasta su nueva residencia y los seguimos de cerca.
                    </p>
                </div>

                <div class="col col-md-6 text-center pr-3 pt-4 infos" data-sal="slide-up" data-sal-delay="700"
                     data-sal-easing="ease-out-back">
                    <p><i class="fas fa-award infos_icon"></i><br>
                    <h2>Excelencia</h2>
                    Las universidades con las que trabajamos son todas rankeadas a nivel mundial.
                    Todas cuentan con estándares académicos de excelencia y con instalaciones y residencias
                    universitarias. Como ejemplo estas universidades poseen una mayor cantidad de programas de maestría
                    y doctorados do que de licenciaturas…
                    </p>
                </div>
            </div>
        </div>

    </div>


    <div class="container text-center">
        <div class="row mt-5 text-center" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="200" data-sal-easing="ease-out-back">
            <div class="col"><p style="font-size:24px;font-weight: 700;">Alianzas</p></div>
            
        </div>

        <div class="row text-center">

            <div class="col col-lg-3 col-sm-6 col-12 p-2" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="300" data-sal-easing="ease-out-back">
                <img src="{{asset('media/iscte.png')}}" title="Instituto Universitário de Lisboa" alt="Logótipo - Instituto Universitário de Lisboa" style="width:100%;max-width:200px">
            </div>

            <div class="col col-lg-3 col-sm-6 col-12 p-2" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="400" data-sal-easing="ease-out-back">
                <img src="{{asset('media/Ualg.png')}}" title="Universidade do Algarve" alt="Logótipo - Universidade do Algarve" style="width:100%;max-width:200px">
            </div>

            <div class="col col-lg-3 col-sm-6 col-12 p-2" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="500" data-sal-easing="ease-out-back">
                <img src="{{asset('media/UBI.png')}}" title="Universidade Beira Interior" alt="Logótipo - Universidade Beira Interior" style="width:100%;max-width:200px">
            </div>

            <div class="col col-lg-3 col-sm-6 col-12 p-2" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="600" data-sal-easing="ease-out-back">
                <img src="{{asset('media/Nautica.png')}}" title="Escola Superior Náutica" alt="Logótipo - Escola Superior Náutica" style="width:100%;max-width:200px">
                
            </div>

        </div>
      
    </div>
<br><br>
{{--     <div class="container-fluid text-center pt-3 pb-5" >
        <div class="container">
            <div class="row">

                <div class="col card shadow-sm p-5" style="background-color:#4D799F">
                    <!-- 3 section -->
                    <h3 class="font-weight-bold text-white mb-5 textos_comuns" style="text-align: center" data-sal="slide-up"
                    data-sal-delay="300" data-sal-easing="ease-out-back">
                    ¿Quieres conocernos mejor?
                    </h3>
                    <a href="{{route('about')}}" class="btn btn-lg btn-light btn-rounded btn-sabermais" data-sal="slide-up"
                    data-sal-delay="300" data-sal-easing="ease-out-back">¡Si, quiero saber más!</a>
                </div>

            </div>
        </div>
    </div> --}}

   <!--Ficheiro de JS-->
    <script type="text/javascript" src="/js/script-about.js"></script>
    <script type="text/javascript" src="/js/script-front-index.js"></script>
@endsection
