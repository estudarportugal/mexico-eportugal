<!DOCTYPE html>
<html lang="pt" dir="ltr">


    @include('frontoffice.partials.header')



<body>
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>

    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    
    <script src="{{asset('vendor/sal/dist/sal.js')}}"></script>

    @include('frontoffice.partials.navbar')

    @yield('content')

    @include('frontoffice.partials.footer')

</body>

</html>
