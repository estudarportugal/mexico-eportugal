@extends('frontoffice.master-front')

@section('title', 'Contacto')

@section('content')

    <!-- CSS Link -->
    <link href="{{asset('css/style-contacts.css')}}" rel="stylesheet">

    @include('frontoffice.partials.header')

    <div class="hero-image">
      <div class="container">
        <div class="hero-text" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="100" data-sal-easing="ease-out-back">
          <h1>¿Tienes alguna pregunta?</h1>
          <p>¡No dudes en contactarnos!<br>Haremos todo lo posible para que nuestra respuesta sea breve y clara.</p>
          <br>
          <a type="button" href="#container">Contacto</a>
        </div>
      </div>
    </div>
    <div id="container">
      <div id="main">
        <div class="container">
        <div class="row">
            <div class="col-md-6">
              <h3>Contactos</h3><br><br>
              <p>
                <b>Estás listo para cambiar el ritmo de tu vida?</b><br>Habla con nosotros a través de los contactos disponibles,<br>o envía tu mensaje en el siguiente formulario.
              </p><br><br>
              <p>
                <b>Dirección:</b><br>Avenida do Atlântico, Edifício Panoramic,<br>Escritório 8, Nº 16, 14º <br>1990-019 Lisboa
              </p><br><br>
              <p>
                <b>Móviles:</b><br>+351 961 326 370 <br>+351 961 326 971
              </p><br><br>
              <p>
                <b>E-mail:</b><br>estudarportugal@gmail.com
              </p><br><br>
              <p>
                ¡Sigue en las redes sociales!
              </p><br>
              <a href="https://www.facebook.com/EstudarPortugalPT" class="icon-bg" target="_blank">
                <i class="fab fa-facebook-f"></i>
              </a>
              <a href="https://www.instagram.com/estudarportugalpt/" class="icon-bg" target="_blank">
                <i class="fab fa-instagram"></i>
              </a>
              <a href="" class="icon-bg" target="_blank">
                <i class="fab fa-twitter"></i>
              </a>
              <a href="" class="icon-bg" target="_blank">
                <i class="fab fa-youtube"></i>
              </a>
            </div>
            <div class="col-md-6" id="contact-form">
              <h3>¡Habla con nosotros!</h3>
              <br><br>

              <?php
              if (empty($_POST["email"])) {
                $emailErr = "Email is required";
              } else {
                $email = test_input($_POST["email"]);
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                  $emailErr = "Invalid email format";
                }
              }
              ?>

              <form method="post" action="{{route('sendmail')}}">
                @csrf
                @method('GET')
                <input class="input-form" type="text" name="name" required placeholder="Nombre Completo (Obligatorio)"><br><br>
                <input class="input-form" type="email" name="email" pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" required placeholder="E-mail (Obligatorio)"><br><br>
                <input class="input-form" type="text" name="phone" required placeholder="Móvil (Obligatorio)"><br><br>
                <input class="input-form" type="text" name="academicStatus" required placeholder="Nivel Educativo (Obligatorio)"><br><br>
                <input class="input-form" type="text" name="currentSchool" required placeholder="Colegio / Escuela / Universidad actual (Obligatorio)"><br><br>
                <textarea name="test" rows="5" cols="49" required placeholder="Para te poder ayudar mejor: La area que te gusta, la carrera, que quieres estudiar? (Obligatorio)"></textarea>
                <br><br>
                <div id="formButtons">
                  <input id="resetBtn" type="reset" value="Limpiar">
                  <input id="submitBtn" type="submit" value="Enviar">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
