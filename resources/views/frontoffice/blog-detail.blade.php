@extends('frontoffice.master-front')

@section('title', $post->title)

@section('tags', $post->tags)

@section('content')

    <!-- CSS Link -->
    <link href="{{asset('css/style-blog-detail.css')}}" rel="stylesheet">

    @include('frontoffice.partials.header')

    <div class="hero-image"
         style="background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('{{ asset('/storage/posts-images/'.$post->image) }}')">
        <div class="container">
            <div class="hero-text" data-sal="slide-up" data-sal-duration="1000" data-sal-delay="100"
                 data-sal-easing="ease-out-back">
                <h1>{{$post->title}}</h1>
                <p>{{$post->resume}}</p>
                <br>
                <a type="button" href="#container">Ler mais</a>
            </div>
        </div>
    </div>
    <div id="container">
        <div id="main">
            <div class="container">
                <br>
                <p>{!! $post->description !!}</p><br>
                <div class="row">
                    @if($post->video === null)
                    @else
                        <video class="col-lg-12 mt-2 mb-5" controls>
                            <source src="{{Storage::disk('public')->url('posts-videos/').$post->video}}"
                                    type="video/mp4">
                        </video>
                    @endif
                    <div class="col-md-12">
                        <p><b>Publicado:</b> {{date('d/m/Y', strtotime($post->created_at) )}}</p>
                        @isset($post->location)<p><b>Localización:</b> {{$post->location}}</p>@endisset
                    </div>
                </div>
                <br>
                <hr>
                <br>
                <h4>Comentários</h4><br><br>
                <div id="create-comment-section" class="row">
                    <div class="col-md-1">
                    <span>
                        <i class="fas fa-user"></i>
                    </span>
                    </div>
                    <div class="col-md-11" id="comment_div">
                        <form method="POST" action="{{route('storeComment')}}" enctype="multipart/form-data">
                            <!-- CSRF Protection -->
                            @csrf
                            <br>
                            <input type="hidden" name="id_post" value="{{$post->id}}">
                            <input class="input-form" type="text" name="name" placeholder="Nombre">
                            <input class="input-form" type="text" name="email" placeholder="E-mail"><br><br>
                            <textarea name="comment" rows="1" cols="80" placeholder="Escreva o seu comentário"></textarea>
                            <br><br>
                            <div id="formButtons">
                                <input id="resetBtn" type="reset" value="Limpar">
                                <input id="submitBtn" type="submit" value="Enviar">
                            </div>
                        </form>
                    </div>
                </div>
                <br><br>
                <hr>
                <br>
                <p><b>
                        @if ($totalcomments->quant == 1)
                            {{$totalcomments->quant}} Comentário
                        @else
                            {{$totalcomments->quant}} Comentários
                        @endif
                    </b></p>
                <br>
                @if ($comments == null)
                    Sem comentários
                @else
                    @foreach($comments as $comment)
                        <div id="comment-section" class="row mt-4 mb-4">
                            <div class="col-md-1">
                                <span><i class="fas fa-user"></i></span>
                            </div>
                            <div id="comment" class="col-md-11">
                                <p><b>{{$comment->name}}</b></p>
                                <p>{{$comment->comment}}</p>
                                <p class="text-muted">
                                    <small>Criado em: {{date('d/m/Y', strtotime($comment->created_at))}}</small></p>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection
