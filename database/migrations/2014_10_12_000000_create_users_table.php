<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->enum('role', ['Admin', 'Employer']);
            $table->softDeletes();
            $table->timestamps();
        });

        $password = Hash::make('admin');

        $data = array(
            array('id'=>'1', 'name'=> 'Administrador', 'password'=> $password, 'email'=>'admin@test.com','role'=>'Admin','created_at'=>'2020-02-12 00:00:00','updated_at'=>'2020-02-12 00:00:00'),
        );

        DB::table('users')->insert($data); // Query Builder approach
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
