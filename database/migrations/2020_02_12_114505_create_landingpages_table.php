<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLandingpagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landingpages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->text('description');
            $table->string('tags');
            $table->string('backgroundimage1')->default('landingpage-image.jpg');
            $table->string('backgroundimage2')->default('img4.jpg');
            $table->string('contact1');
            $table->string('contact2');
            $table->timestamps();
            $table->softDeletes();
        });

        $data = array(
            array('id'=>'1', 'title'=> 'No pospongas tu futuro.','tags'=> 'Futuro, Estudantes', 'contact1' => '(+521) 553 668 76 43', 'contact2'=>'(+351) 917 804 598',
                'description'=>'¡Ven a vivir y estudiar en el país que te encantará por las oportunidades que te brindará!', 'created_at'=>'2019-1-30 00:00:00','updated_at'=>'2019-1-30 00:00:00'),
        );

        DB::table('landingpages')->insert($data);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('landingpages');
    }
}
