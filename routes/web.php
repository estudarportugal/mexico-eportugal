<?php

// Front-office -> Routes

// Index
Route::get('/', 'PageController@index')->name('index');

// Sobre
Route::get('/acerca', 'PageController@acerca')->name('about');

// Contactos
Route::get('/contacto', 'ContactController@index')->name('contacts');
Route::get('/contacto/sendemail', 'ContactController@sendMail')->name('sendmail');

// Blog
Route::get('/blog', 'BlogController@index')->name('blog');
Route::get('/blog/{post}', 'BlogDetailController@index')->name('blogDetail');
Route::post('/blog', 'CommentController@store')->name('storeComment');

// Landing Page
Route::get('/landingpage', 'LandingPageController@index')->name('landingPage');

// Back-Office -> Routes

Auth::routes();

Route::get('/accountconfirmation/{user}', 'ConfirmationController@index')->name('confirmation.index');
Route::post('/accountconfirmation/{user}', 'ConfirmationController@update')->name('confirmation.update');

Route::group(['prefix' => 'backoffice', 'middleware' => ['auth','RevalidateBackHistory']], function () {

    // Home
    Route::get('/', 'HomeController@index')->name('home');

    // Ajuda
    Route::get('/ajuda', 'HomeController@ajuda')->name('help');

    // Publicações - Blog
    Route::resource('/posts', 'PostController');

    // Comentários - Blog
    Route::resource('/comments', 'CommentController');
/*  Route::get('/comments', 'CommentController@index')->name('comments');
    Route::get('/comments/{$comment_id}', 'CommentController@show')->name('showcomments'); */

    // landing Page - Blog
    Route::resource('/landingpages', 'LandingPageController');

    // Testimonial - Blog
    Route::resource('/testimonials', 'TestimonialController');

    // Users
    Route::resource('/users', 'UserController');

    // Logout
    Route::get('/logout', 'Auth/LoginController@logout');
});
