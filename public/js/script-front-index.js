$(document).ready(function () {

    /* Botão inscrição */
    $("#btn_inscripcion").click(function(){


        $("#modal-title").text("Inscripción");

        $("#modal_icon").removeClass();
        $("#modal_icon").addClass( "fa fas fa-pen-square" );

        $("#modal_message").text("Para iniciar tu camino hacia Portugal, hacia Europa te deberás inscribir junto de nuestros representantes. Para te inscribir, además de algunos documentos personales, te bastará tener un documento de constancia que ateste tu nivel de estudios actuales, así como un documento de alguno examen para ingreso al ensino superior que hayas rendido.");

        $('#myModal').modal()
      });



    /* Botão postulation */
    $("#btn_postulation").click(function(){


        $("#modal-title").text("Postulation");

        $("#modal_icon").removeClass();
        $("#modal_icon").addClass( "fa fas fa-file-contract" );

        $("#modal_message").text("Para poder postular habrá que cumplir algunos requisitos esenciales. Para licenciatura, habrá que haber concluido el bachillerato (preparatorio), así como de mínimo haber rendido el examen EXANI II en la área o carrera a que apuntas. Para maestría vas a necesitar de tu título de licenciatura, y para PhD será necesario contar con el título de tu maestría y tu curricular.");

        $('#myModal').modal()
      });



    /* Botão matricula */
    $("#btn_matricula").click(function(){


        $("#modal-title").text("Monitoreo de cupos");

        $("#modal_icon").removeClass();
        $("#modal_icon").addClass( "fa far fa-address-card" );

        $("#modal_message").text("¡El ingreso a las universidades se hace siempre por mérito! Para eso el equipo de asesores de Estudar Portugal estará continuamente monitoreando las mejores posibilidades que tu tendrás. Es una gran ventaja poder saber adónde se podrá matricular con suceso.");

        $('#myModal').modal()
      });



    /* Botão Viaje */
    $("#btn_viaje").click(function(){


        $("#modal-title").text("Visa");

        $("#modal_icon").removeClass();
        $("#modal_icon").addClass( "fa fas fa-suitcase-rolling" );

        $("#modal_message").text("Para venir a estudiar para Portugal es necesario una VISA estudiantil. Estudar Portugal como organización de asesoría académica mantiene una relación muy cerca con universidades y embajadas portuguesas para te poder apoyar en todos los momentos. Sin embargo, la VISA es y será siempre un proceso personal e intransmisible!");

        $('#myModal').modal()
      });



    /* Botão ViajePT */
    $("#btn_viajept").click(function(){


        $("#modal-title").text("Viaje y Acogida en Portugal");

        $("#modal_icon").removeClass();
        $("#modal_icon").addClass( "fa fas fa-plane-departure" );

        $("#modal_message").text("Estudar Portugal te estará esperando en aeropuerto para te recibir y trasladar hacia tu nueva habitación en tu nueva casa :) Te garantizamos la seguridad, la tranquilidad y el conforto para que tu nueva vida sea lo mejor posible.");

        $('#myModal').modal()
      });


});


