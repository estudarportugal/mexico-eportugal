$(document).ready(function () {

//Contador Estudantes

//Buscar o Número a Aplicar o Efeito, e transforma-lo de string para Inteiro
var number = parseInt(document.getElementById("number").innerHTML);

//Definir o Número que Está a ser Mostrado na Página para 0 para dar Inicio ao Efeito
document.getElementById("number").innerHTML = 0;

//Variável que terá o Valor a Apresentar na Página
var valorFinal_number = 0;

//Dividir o Número em Pedaços,
//para poder mostrar Números Maiores Mais Rapidamente
//E com a Mesma Velocidade que Números Menores
var add_number = number / 21;

var countUp_number = setInterval(function () {

    //Caso Ainda Não se Tenha Chegado ao Valor Final
    if (Math.round(valorFinal_number) != number) {

        //Ir Juntado os Pedaços para Formar o Número Final
        valorFinal_number = valorFinal_number + add_number;

        //Arredondar o Número para Nunca Mostrar Valores como 4325.374
        document.getElementById("number").innerHTML = Math.round(valorFinal_number);

    } else {
        //Caso se Tenha Chegado ao Valor Final
        clearInterval(countUp_number);
    }

}, 60);

//Contador Universidades

//Buscar o Número a Aplicar o Efeito, e transforma-lo de string para Inteiro
var number1 = parseInt(document.getElementById("number1").innerHTML);

//Definir o Número que Está a ser Mostrado na Página para 0 para dar Inicio ao Efeito
document.getElementById("number1").innerHTML == 0;

//Variável que terá o Valor a Apresentar na Página
var valorFinal_number1 = 0;

//Dividir o Número em Pedaços,
//para poder mostrar Números Maiores Mais Rapidamente
//E com a Mesma Velocidade que Números Menores
var add_number1 = number1 / 21;

var countUp_number1 = setInterval(function () {

    //Caso Ainda Não se Tenha Chegado ao Valor Final
    if (Math.round(valorFinal_number1) != number1) {

        //Ir Juntado os Pedaços para Formar o Número Final
        valorFinal_number1 = valorFinal_number1 + add_number1;

        //Arredondar o Número para Nunca Mostrar Valores como 4325.374
        document.getElementById("number1").innerHTML = Math.round(valorFinal_number1);

    } else {
        //Caso se Tenha Chegado ao Valor Final
        clearInterval(countUp_number1);
    }

}, 60);

//Contador Paises

//Buscar o Número a Aplicar o Efeito, e transforma-lo de string para Inteiro
var number2 = parseInt(document.getElementById("number2").innerHTML);

//Definir o Número que Está a ser Mostrado na Página para 0 para dar Inicio ao Efeito
document.getElementById("number2").innerHTML == 0;

//Variável que terá o Valor a Apresentar na Página
var valorFinal_number2 = 0;

//Dividir o Número em Pedaços,
//para poder mostrar Números Maiores Mais Rapidamente
//E com a Mesma Velocidade que Números Menores
var add_number2 = number2 / 21;

var countUp_number2 = setInterval(function () {

    //Caso Ainda Não se Tenha Chegado ao Valor Final
    if (Math.round(valorFinal_number2) != number2) {

        //Ir Juntado os Pedaços para Formar o Número Final
        valorFinal_number2 = valorFinal_number2 + add_number2;

        //Arredondar o Número para Nunca Mostrar Valores como 4325.374
        document.getElementById("number2").innerHTML = Math.round(valorFinal_number2);

    } else {
        //Caso se Tenha Chegado ao Valor Final
        clearInterval(countUp_number2);
    }

}, 60);



});


