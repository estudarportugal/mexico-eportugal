$(document).ready(function () {
    $('#dataTable').DataTable({
        "columnDefs": [
            {"orderable": false, "targets": 4},


            {"width": "120px", "targets": 0},

            {"width": "130px", "targets": 3},
            {"width": "130px", "targets": 4}

        ],
        "order": [[0, "desc"]],

        "language": {'url': '//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese.json'}
    });



    var formToSubmit //Varial para indicar o forumulário a submeter

    $(".form_comment_id").submit(function(e){
        formToSubmit = this;
        $("#spanUserId").text(this.id);
        return false;
    });


    $(".btn_submit").click(function(e){
        formToSubmit.submit();
    });







});


