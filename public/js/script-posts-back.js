// Funcionalidade de escolher a imagem
$(document).on("click", ".browse", function () {

    var file = $(this).parents().find(".file");
    file.trigger("click");

    $('input[type="file"]').change(function (e) {
        var fileName = e.target.files[0].name;


        // Verifica se o tamanho da imagem não passa os 2MB
        if (e.target.files[0].size > 2097152) { //Bytes
            $("#fileHelp").addClass("warning_img_size");
            $("#fileHelp").focus();
            fileName = null;

            // Se a imagem for maior que 2MB, repoem a img default
            document.getElementById("preview").src = "/media/logo-text.png";
            $("#inputImage").val(null);

            return;
        } else {
            $("#fileHelp").removeClass("warning_img_size");
        }

        $("#file").val(fileName);
    });
});


// Funcionalidade de escolher a video
$(document).on("click", ".browse", function () {

    var fileVideo = $(this).parents().find(".file");
    fileVideo.trigger("click");

// Funcionalidade de escolher a imagem
    $('input[type="file"]').change(function (v) {
        var fileNameVideo = v.target.files[0].name;

        // Verifica se o tamanho da imagem não passa os 2MB
        if (v.target.files[0].size > 79962499) { //Bytes
            $("#fileHelpVideo").addClass("warning_img_size");
            $("#fileHelpVideo").focus();
            fileNameVideo = null;

            // Se a imagem for maior que 2MB, repoem a img default
            document.getElementById("preview").src = "/media/logo-text.png";
            $("#inputVideo").val(null);

            return;
        } else {
            $("#fileHelpVideo").removeClass("warning_img_size");
        }

        $("#fileVideo").val(fileNameVideo);
    });
});

