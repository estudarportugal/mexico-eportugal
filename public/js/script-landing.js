// Funcionalidade de escolher a imagem1
$(document).on("click", ".browse", function () {

    var file1 = $(this).parents().find(".file");
    file1.trigger("click");

    $('input[type="file"]').change(function (e) {
        var fileName1 = e.target.files[0].name;


        // Verifica se o tamanho da imagem não passa os 2MB
        if (e.target.files[0].size > 3097152) { //Bytes
            $("#fileHelp1").addClass("warning_img_size");
            $("#fileHelp1").focus();
            fileName1 = null;

            // Se a imagem for maior que 2MB, repoem a img default
            document.getElementById("preview").src = "/media/landingpage-image.jpg";
            $("#inputImage1").val(null);

            return;
        } else {
            $("#fileHelp1").removeClass("warning_img_size");
        }

        $("#file1").val(fileName1);
    });
});

// Funcionalidade de escolher a imagem1
$(document).on("click", ".browse", function () {

    var file2 = $(this).parents().find(".file");
    file2.trigger("click");

    $('input[type="file"]').change(function (e) {
        var fileName2 = e.target.files[0].name;


        // Verifica se o tamanho da imagem não passa os 2MB
        if (e.target.files[0].size > 3097152) { //Bytes
            $("#fileHelp2").addClass("warning_img_size");
            $("#fileHelp2").focus();
            fileName2 = null;

            // Se a imagem for maior que 2MB, repoem a img default
            document.getElementById("preview").src = "/media/img4.jpg";
            $("#inputImage2").val(null);

            return;
        } else {
            $("#fileHelp2").removeClass("warning_img_size");
        }

        $("#file2").val(fileName2);
    });
});
