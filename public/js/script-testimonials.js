$(document).ready(function () {
    $('#dataTable').DataTable({
        "columnDefs": [
            {"orderable": false, "targets": 0},
            {"orderable": false, "targets": 3},

            {"width": "110", "targets": 0},
            {"width": "130", "targets": 2},
            {"width": "130", "targets": 3}


        ],
        "order": [[2, "desc"]],

        "language": {'url': '//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese.json'}
    });



    var formToSubmit //Varial para indicar o forumulário a submeter

    $(".form_testimonial_id").submit(function(e){
        formToSubmit = this;
        return false;
    });


    $(".btn_submit").click(function(e){
        formToSubmit.submit();
    });







});


